﻿namespace Dating_App;

internal partial class PaintForm : Form
{
    protected Pen p = new(Color.Black, 5);
    protected Bitmap bmp;
    protected Graphics gr;
    protected bool _clicked;
    protected string _interest;
    protected int _penThickness = 5;

    public PaintForm(string n)
    {
        InitializeComponent();
        bmp = new Bitmap(canvas.Width, canvas.Height);
        prepareCanvas();
        black.Checked = true;
        _interest = n;
    }

    private void prepareCanvas() //This makes sure the canvas is white and not transparent.
    {
        gr = Graphics.FromImage(bmp);
        gr.Clear(Color.White);
    }

    public virtual void savedraw_Click(object sender, EventArgs e) //Saves the drawing to the user's folder.
    {
        if (!Directory.Exists(Database.CurrentUserImagesDir))
        {
            Directory.CreateDirectory(Database.CurrentUserImagesDir);
        }
        string fileDirName = $@"{Database.CurrentUserImagesDir}{_interest}.png";
        try
        {
            File.Delete(fileDirName);
            bmp.Save(fileDirName, System.Drawing.Imaging.ImageFormat.Png);
            Database.CurrentUser.UserImages[int.Parse(_interest)] = bmp;
        }
        catch (IOException)
        {
            MessageBox.Show("file already exists on disk, ignoring change...", "Error");
        }
        Close();
    }

    #region User Painter Controls
    private void canvas_MouseUp(object sender, MouseEventArgs mouse) { _clicked = false; }
    private void canvas_MouseDown(object sender, MouseEventArgs mouse)
    {
        _clicked = true;
    }
    private void canvas_MouseMove(object sender, MouseEventArgs mea)
    {
        canvas.Image = bmp;
        if (_clicked == true)
        {
            gr.FillEllipse(new SolidBrush(p.Color), mea.X-(_penThickness/2), mea.Y-(_penThickness/2), _penThickness, _penThickness);
        }
        canvas.Invalidate();
    }

    private void black_CheckedChanged(object sender, EventArgs e) { p.Color = Color.Black; }
    private void white_CheckedChanged(object sender, EventArgs e){p.Color = Color.White;}
    private void blue_CheckedChanged(object sender, EventArgs e){p.Color = Color.Blue;} 
    private void red_CheckedChanged(object sender, EventArgs e){p.Color = Color.Red;}
    private void green_CheckedChanged(object sender, EventArgs e){p.Color = Color.Green;}
    private void yellow_CheckedChanged(object sender, EventArgs e) { p.Color = Color.Yellow; }

    private void trackBarThickness_Change(object sender, EventArgs e)
    {
        labPenSize.Text = (trackBarThickness.Value).ToString();
        _penThickness = trackBarThickness.Value;
    }

    private void clearcanvas_Click(object sender, EventArgs e) //Clears the canvas.
    {
        gr.Clear(Color.White);
        canvas.Invalidate();
    }
    #endregion
}