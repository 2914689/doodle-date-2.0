﻿namespace Dating_App;

internal partial class helpPage : Form
{ 
    public helpPage()
    {
        InitializeComponent();
        Tools.displayPFP(pictureBox1);
    }

    #region Page Buttons || These allow the user to switches to the other menus.
    private void helpHome_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form menuPage = new menuPage();
        menuPage.ShowDialog();
        this.Close();
    }

    private void helpMatch_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form matchPage = new matchPage();
        matchPage.ShowDialog();
        this.Close();
    }

    private void helpChat_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form chatPage = new chatPage();
        chatPage.ShowDialog();
        this.Close();
    }

    private void helpLogin_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form loginPage = new loginPage();
        loginPage.ShowDialog();
        this.Close();
    }
    #endregion

    private void label1_Click(object sender, EventArgs e)
    {

    }
}