﻿using System.Diagnostics;

namespace Dating_App.Controllers;

internal static class Database
{
    [DebuggerBrowsable(DebuggerBrowsableState.Never)] //should prevent a time-out during runtime when using a watch on CurrentUser -E
    public static UserProfile CurrentUser { get; set; }
    public static Dictionary<string, string> Credentials { get; set; } = new Dictionary<string, string>(); //<username, password>
    public static List<UserProfile> AllUserProfiles { get; set; } = new List<UserProfile>();
    public static string[] Genders { get; } = {"Male", "Female", "Non-Binary"}; //TODO 70: implement -E
    public static string[] Orientations { get; } = { "Heterosexual", "Homosexual", "Bisexual" }; //TODO 70: implement -E\
    public static string[] allInterests { get; set; } = {
        "Music",
        "Football",
        "Movies",
        "Mountains",
        "Instagram",
        "E-Sports",
        "Gaming",
        "Dancing",
        "Painting",
        "Reading",
        "Skiing",
        "Cosplay",
        "Tiktok",
        "Makeup",
        "Politics",
        "Anime",
        "Clubbing",
        "Motorcycle",
        "Disney",
        "Travel",
        "Cars",
        "Camping",
        "Astrology",
        "Rave",
        "Theater",
        "Basketball",
        "Bowling",
        "Marvel",
        "Cooking",
        "Soccer",
        "Board Games",
        "Gym",
        "Drawing",
        "Dungeons & Dragons",
        "Gardening",
        "Singing",
        "Hockey",
        "Gymnastics",
        "Tennis",
        "Stocks"
    };

    //general directories
    public static string WorkingDir { get; } = Directory.GetCurrentDirectory();
    public static string UserDataDir { get; } = WorkingDir + @"\UserData\";
    public static string UserImagesDir { get; } = UserDataDir + @"UserImages\";
    public static string CurrentUserImagesDir { get; set; }

    //DateTimePicker clamp
    public static readonly DateTime AgeClamp = DateTime.Today.AddYears(-18);

    private static List<string> _excludedAccounts { get; set; } = new List<string>(); //remember ignored users if the user backs out of the main program and tries to reenter
    
    public static void initialiseDatabase()
    {
        loadProfiles();
        loadCredentials();
    }

    static void loadProfiles() //from disk into memory
    {
        AllUserProfiles.Clear();
        string[] files = Directory.GetFiles(UserDataDir, "*.txt");
        for (int i = 0; i < files.Length; i++)
        {
            string username = Path.GetFileNameWithoutExtension(files[i]);
            if (!_excludedAccounts.Contains(username))
            {
                try
                {
                    AllUserProfiles.Add(new UserProfile(username));
                }
                catch (IndexOutOfRangeException e)
                {
                    if (MessageBox.Show(
                            $"Account associated with username: \"{username}\" seems to be corrupted. Internal errormessage: {e.Message} Press \"Yes\" to delete account from disk or press \"No\" to skip processing the account",
                            "Error", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        removeAccount(username);
                    }
                    else
                    {
                        _excludedAccounts.Add(username);
                    }
                }
            }
        }
    }
    static void removeAccount(string username) //removes account correlated with the username from disk
    {
        File.Delete($@"{Database.UserDataDir}{username}.txt");
    }
    static void loadCredentials() //Loads all the existing username/password pairs into a dictionary. //TODO 70: this method, and the field above, doesn't add much of anything, and could just be removed. But it stays for now as I like the idea of seperating the profiles somewhat for the username/password verification
    {
        Credentials.Clear();
        foreach (var u in AllUserProfiles)
        {
            Credentials.Add(u.Username, u.Password);
        }
    }
}

