﻿

using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace Dating_App.Controllers;

internal class UserProfile
{
    public string Username { get; set; }
    public string Password { get; set; }
    public string FullName { get; set; }
    public DateOnly DateOfBirth { get; set; }
    public string AgePreference { get; set; }
    public string Gender { get; set; }
    public string Orientation { get; set; }
    public string[] UserInterests { get; set; } = new string[8];
    public Image[] UserImages { get; set; } = new Image[8];
    public string eMail { get; }
    public UserProfile(string username) //load from disk
    {
        string userFileDir = $@"{Database.UserDataDir}{username}.txt";
        string[] lines = File.ReadAllLines(userFileDir);
        string line = Encryption.Crypt(lines[0], 'D');
        string[] information = line.Split(new string[] { "//" }, StringSplitOptions.None); //check for empty fields
        Username = information[0];
        Password = information[1];
        FullName = information[2];
        DateOfBirth = DateOnly.Parse(information[3]); //DateTime.Parse(information[3]); //TODO: datetime 1/2
        AgePreference = information[4];
        Gender = information[5];
        Orientation = information[6];
        for (int i = 0; i <= 7; i++)
        {
            UserInterests[i] = information[i + 7];
        }
        eMail = $"{Username}@gmail.com";
        LoadDrawings(Username);
    }
    void LoadDrawings(string u) //Loads the drawings into userprofile
    {
        if (!Directory.Exists($@"{Database.UserImagesDir}{u}"))
        {
            Directory.CreateDirectory($@"{Database.UserImagesDir}{u}");
            return;
        }
        string[] files = Directory.GetFiles($@"{Database.UserImagesDir}{u}", "*.png");
        string imageName;
        for (int i = 0; i < files.Length; i++)
        {
            imageName = Path.GetFileNameWithoutExtension(files[i]);

            string images = Path.GetFileNameWithoutExtension(files[i]);
            UserImages[int.Parse(images)] = Image.FromFile(files[i]);//breaks overwriting saved images

            /* //fix for overwrite error.Doesnt work currently, credit:https://stackoverflow.com/questions/39264539/overwrite-bmp-file-cant-delete-because-its-used-by-another-process-dispose
            Image image = null;
            try
            {
                using (var imageFromFile = Image.FromFile(files[i])) 
                {
                    image = new Bitmap(imageFromFile);
                }

                UserImages[int.Parse(imageName)] = image;
            }
            finally
            {
                using (image){}
            }
            */
        }
    }
}