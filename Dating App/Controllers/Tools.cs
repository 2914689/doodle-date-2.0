﻿using System.Windows.Forms;

namespace Dating_App.Controllers;

internal class Tools
{
    public static string appendStringArray(StringBuilder sb, string[] sArray)
    {
        for (int i = 0; i < sArray.Length; i++)
        {
            sb.Append(sArray[i] + @"//");
        }

        string result = sb.ToString();
        return result;
    }
    public static void displayImages(GroupBox gb, Image[] imgArray)
    {
        try
        {
            foreach (PictureBox pb in gb.Controls)
            {
                if (imgArray[int.Parse(pb.Tag.ToString())] != null)
                {
                    pb.Image = imgArray[int.Parse(pb.Tag.ToString())];
                }
            }
        }
        catch{ }
        
    }
    public static void displayProfilePictures(GroupBox gb, Image[] imgArray)
    {
        foreach (PictureBox pb in gb.Controls)
        {
            if (imgArray[0] != null)
            {
                pb.Image = imgArray[0];
            }
        }
    }
    public static void displayPFP(PictureBox pb)
    {
        pb.Image = Database.CurrentUser.UserImages[0];
    }
}