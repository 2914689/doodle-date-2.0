﻿using System.Linq;

namespace Dating_App.Controllers;

internal static class Matching
{
    static Dictionary<string, int> _allMatches = new Dictionary<string, int>(); //<matched user, matchpercentage>
    public static List<string> noMatch = new List<string>();
    static Dictionary<string, int> _remainingMatches = new Dictionary<string, int>();
        
    public static UserProfile CurrentMatch { get; set; }
    public static List<string> allMadeMatches { get; set; } = new ();
    public static string CurrentMatchImagesDir { get; set; }
    static bool _startUp = true;

    static Matching()
    {
        determineAllMatches();
    }
        
    public static void makeCurrentMatch()
    {
        if (!_startUp)
        {
            noMatch.Add(CurrentMatch.Username);
        }
        _startUp = false;
        determineRemainingMatches();
        if (!noRemainingMatches())
        {
            string username = _remainingMatches.OrderByDescending(x => x.Value).First().Key;
            CurrentMatch = new UserProfile(username);
            CurrentMatchImagesDir = Database.UserImagesDir + $"{CurrentMatch.Username}";
        }
    }

    static void determineRemainingMatches()
    {
        _remainingMatches.Clear();
        foreach (KeyValuePair<string, int> username in _allMatches)
        {
            if (!noMatch.Contains(username.Key) && !allMadeMatches.Contains(username.Key))
            {
                _remainingMatches.Add(username.Key, username.Value);
            }
        }
    }

    static void determineAllMatches()
    {
        foreach (UserProfile u in Database.AllUserProfiles)
        {
            if (!_allMatches.ContainsKey(u.Username) || (u.Username != Database.CurrentUser.Username))
            {
                if (checkGenderCompatible(Database.CurrentUser, u))
                {
                    _allMatches.Add(u.Username, matchInterests(Database.CurrentUser, u));
                }
            }
                
        }
    }
    static bool noRemainingMatches()
    {
        if (!_remainingMatches.Any())
        {
            if (MessageBox.Show(
                    "You have run though all other compatible users! Would you like to reset your declined matches?",
                    "Error", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                noMatch.Clear(); //TODO 65: these could be placed in a earlier stack call -E
                determineRemainingMatches();
                return false;
            }
            return true;
        }
        return false;
    }

    static bool checkGenderCompatible(UserProfile u1, UserProfile u2)
    {
        if (checkGenderPrefOneWay(u1, u2) && checkGenderPrefOneWay(u2, u1))
            return true;
        { return false; }
    }

    static bool checkGenderPrefOneWay(UserProfile u1, UserProfile u2) //determine all possible matches, and gives their matchingspercentage
    {
        //TODO 99: could be more optimised -E
        switch (u1.Gender)
        {
            case "Male":
                if ((u1.Orientation == "Heterosexual" && u2.Gender != "Male") ||
                    (u1.Orientation == "Homosexual" && u2.Gender != "Female") ||
                    u1.Orientation == "Bisexual")

                { return true; }
                break;           
            case "Female":
                if ((u1.Orientation == "Heterosexual" && u2.Gender != "Female") ||
                    (u1.Orientation == "Homosexual" && u2.Gender != "Male") ||
                    u1.Orientation == "Bisexual")

                { return true; }
                break;
            case "Non-Binary": return true;
        }

        return false; //TODO 90: the method could never land here without an error being produced in the first place, there must be something that would negate the error produced when this return is omitted -E
    }
    public static void checkAgePref() //TODO 40: make checkagepref
    {
            
    }

    static int matchInterests(UserProfile u1, UserProfile u2)
    {
        int perc = 0;
        for (int i = 0; i < u1.UserInterests.Length; i++)
        {
            for (int j = 0; j < u2.UserInterests.Length; j++)
            {
                if (u1.UserInterests[i] == u2.UserInterests[j])
                {
                    switch (i)
                    {
                        case 0:
                            perc += 16;
                            break;
                        case 1:
                            perc += 15;
                            break;
                        case 2:
                            perc += 14;
                            break;
                        case 3:
                            perc += 13;
                            break;
                        case 4:
                            perc += 12;
                            break;
                        case 5:
                            perc += 11;
                            break;
                        case 6:
                            perc += 10;
                            break;
                        case 7:
                            perc += 9;
                            break;
                    }
                }
            }

        }
        return perc;
    }
}