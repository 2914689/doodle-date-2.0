﻿namespace Dating_App.Controllers;
class Encryption
{
    //Randomly chosen pin
    private static int pin = 26123;

    //Method that returns encrypted or decrypted version of a string
    public static string Crypt(string s, char ed)
    {
        int calc_pin = pin;
        int sign = 1;
        switch (ed)
        {   //Depending on whether the string has to be de- or encrypted the pin has to be added or substracted from the character's ascii value
            case 'E':
                break;
            case 'D':
                sign = -1;
                break;
        }

        string cryp = "";
        foreach (char c in s)
        {   // We use basic Caesar encryption with a twist: the pin is looped through what causes each character to have a different shift than its neighbours
            int asc = (int)c;
            asc += sign * (calc_pin % 10);
            cryp += (char)asc;
            if (calc_pin < 10) calc_pin = pin;
            else { calc_pin = calc_pin / 10; }
        }
        return cryp;
    }
}
