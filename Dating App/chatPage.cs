﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics.Contracts;

namespace Dating_App;
internal partial class chatPage : Form
{
    string[] contacts = { "Ashraf el Attachi", "Beau Käyser", "Denise de Rijcke", "Eon Derkman", "Tessa van Velzen" };
    string LocalIP, RemoteIP, LocalPort = "80", RemotePort;
    Socket sck;
    EndPoint LocalEP, RemoteEP;
    byte[] buffer;

    public chatPage()
    {
        InitializeComponent();
        Tools.displayPFP(pictureBox3);
    }

    #region Page Buttons || These allow the user to switches to the other menus.
    private void chatHome_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form menuPage = new menuPage();
        menuPage.ShowDialog();
        this.Close();
    }

    private void chatMatch_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form matchPage = new matchPage();
        matchPage.ShowDialog();
        this.Close();
    }

    private void chatHelp_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form helpPage = new helpPage();
        helpPage.ShowDialog();
        this.Close();
    }

    private void chatLogin_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form loginPage = new loginPage();
        loginPage.ShowDialog();
        this.Close();
    }
    #endregion

    private void LoadContacts() //Loads the current user's contacts (WIP: we haven't linked specific contacts to specific user's yet.)
    {
        int height = groupContacts.Height;
        int width = groupContacts.Width;
        Size contactSize = new Size(width * 16 / 18, 75);
        for (int i = 0; i < Matching.allMadeMatches.Count; i++)
        {
            UserProfile rbAccount = new UserProfile(Matching.allMadeMatches[i]); //this is very memory intensive
            var rb = new RadioButton();
            rb.Name = $"contact{i}";
            rb.Text = rbAccount.FullName;
            chat.Text = rbAccount.eMail;
            //chat.Text = Database.AllUserProfiles.Find(UserProfile.Username[Matching.allMadeMatches[i]])); less memory intensive to do it  about this way
            msgBox.Clear(); 
            msgList.Items.Clear();
            rb.Size = contactSize;
            rb.Location = new Point(width / 18, 20 + i*contactSize.Height);
            rb.BackColor = Color.Gainsboro;
            rb.Appearance = Appearance.Button;
            groupContacts.Controls.Add(rb);
            rb.Click += (sender, e) => contact_Changed(sender, e, rbAccount);
            rb.Show();
        }
    }

    #region LAN Chat
    private void chatPage_Load(object sender, EventArgs e)
    {
        sck = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        sck.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        LocalIP = GetLocalIP();
        RemoteIP = GetLocalIP();

        LoadContacts();
    }

    private string GetLocalIP()
    {
        IPHostEntry host;
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress IP in host.AddressList)
        {
            if (IP.AddressFamily == AddressFamily.InterNetwork)
            {
                return IP.ToString();
            }
        }
        return "127.0.0.1";
    }

    private void Connect(object sender, EventArgs e)
    {
        LocalEP = new IPEndPoint(IPAddress.Parse(LocalIP), Convert.ToInt32(LocalPort));
        sck.Bind(LocalEP);

        RemoteEP = new IPEndPoint(IPAddress.Parse(RemoteIP), Convert.ToInt32(RemotePort));
        sck.Connect(RemoteEP);

        buffer = new byte[1500];
        sck.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref RemoteEP, new AsyncCallback(MessageCallBack), buffer);
    }

    private void MessageCallBack(IAsyncResult aResult)
    {
        try
        {
            byte[] receivedData = new byte[1500];
            receivedData = (byte[])aResult.AsyncState;

            //Converts the received byte to a readable message
            ASCIIEncoding aEncoding = new ASCIIEncoding();
            string receivedMsg = aEncoding.GetString(receivedData);

            //Add received message to the chat
            string msg = ($"Friend: " + receivedMsg); Message(msg);
            buffer = new byte[1500];
            sck.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref RemoteEP, new AsyncCallback(MessageCallBack), buffer);
        }
        catch (Exception ex)
        {
            msgBox.Text = ex.ToString();
        }
    }
    private void Message(string msg) { if (ControlInvokeRequired(msgList, () => msgList.Items.Add(msg))) return; }
    #endregion

    #region Manual Tool for Network Settings
    private void manual_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
        NetTool.Enabled = true;NetTool.Visible = true;
        IPL.Text = LocalIP; IPR.Text = RemoteIP; PL.Text = LocalPort; PR.Text = RemotePort;
    }
    private void OK_Click(object sender, EventArgs e)
    {
        LocalIP = IPL.Text; RemoteIP = IPR.Text; LocalPort = PL.Text; RemotePort = PR.Text;
        Connect(sender, e);
        NetTool.Enabled = true; NetTool.Visible = false;
    }
    #endregion

    #region User Chat Controls || These allow the user to change contact and send the written message.
    private void send_Click(object sender, EventArgs e)
    {
        //Converts the msg string to a transferable byte
        ASCIIEncoding aEncoding = new ASCIIEncoding();
        byte[] sendingmsg = new byte[1500];
        sendingmsg = aEncoding.GetBytes(msgBox.Text);

        //Sending the byte
        sck.Send(sendingmsg);

        //Adding it to the chat
        msgList.Items.Add($"You: " + msgBox.Text);
        msgBox.Clear();
    }

    private void message_KeyPress(object sender, KeyPressEventArgs e) //Enter key sends the message
    {
        if (e.KeyChar == Convert.ToChar(Keys.Return))
        {
            send_Click(sender, e);
        }
    }
    private void contact_Changed(object sender, EventArgs e, UserProfile u)
    {
        chat.Text = u.eMail;
        msgBox.Clear(); msgList.Items.Clear();
        RemotePort = "81";
    }

    /*
    private void contact1_CheckedChanged(object sender, EventArgs e)
    {
        chat.Text = contact0.Text;
        msgBox.Clear();msgList.Items.Clear();
        RemotePort= "81";
    }

    private void contact2_CheckedChanged(object sender, EventArgs e)
    {
        chat.Text = contact1.Text;
        msgBox.Clear(); msgList.Items.Clear();
        RemotePort = "82";
    }

    private void contact3_CheckedChanged(object sender, EventArgs e)
    {
        chat.Text = contact2.Text;
        msgBox.Clear(); msgList.Items.Clear();
        RemotePort = "83";
    }

    private void contact4_CheckedChanged(object sender, EventArgs e)
    {
        chat.Text = contact3.Text;
        msgBox.Clear(); msgList.Items.Clear();
        RemotePort = "84";
    }

    private void contact5_CheckedChanged(object sender, EventArgs e)
    {
        chat.Text = contact4.Text;
        msgBox.Clear(); msgList.Items.Clear();
        RemotePort = "85";
    }

    private void contact6_CheckedChanged(object sender, EventArgs e)
    {
        chat.Text = contact5.Text;
        msgBox.Clear(); msgList.Items.Clear();
        RemotePort = "86";
    }
    */

    #endregion

    public bool ControlInvokeRequired(Control c, Action a)
    {
        if (c.InvokeRequired) c.Invoke(new MethodInvoker(delegate { a(); }));
        else return false;

        return true;
    }
}