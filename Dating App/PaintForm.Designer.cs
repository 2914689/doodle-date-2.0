﻿namespace Dating_App
{
    partial class PaintForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clearcanvas = new System.Windows.Forms.Button();
            this.savedraw = new System.Windows.Forms.Button();
            this.black = new System.Windows.Forms.RadioButton();
            this.white = new System.Windows.Forms.RadioButton();
            this.red = new System.Windows.Forms.RadioButton();
            this.blue = new System.Windows.Forms.RadioButton();
            this.green = new System.Windows.Forms.RadioButton();
            this.yellow = new System.Windows.Forms.RadioButton();
            this.canvas = new System.Windows.Forms.PictureBox();
            this.trackBarThickness = new System.Windows.Forms.TrackBar();
            this.labPenThickness = new System.Windows.Forms.Label();
            this.labPenSize = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.canvas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThickness)).BeginInit();
            this.SuspendLayout();
            // 
            // clearcanvas
            // 
            this.clearcanvas.Location = new System.Drawing.Point(389, 574);
            this.clearcanvas.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.clearcanvas.Name = "clearcanvas";
            this.clearcanvas.Size = new System.Drawing.Size(107, 60);
            this.clearcanvas.TabIndex = 0;
            this.clearcanvas.Text = "Clear";
            this.clearcanvas.UseVisualStyleBackColor = true;
            this.clearcanvas.Click += new System.EventHandler(this.clearcanvas_Click);
            // 
            // savedraw
            // 
            this.savedraw.Location = new System.Drawing.Point(504, 574);
            this.savedraw.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.savedraw.Name = "savedraw";
            this.savedraw.Size = new System.Drawing.Size(107, 60);
            this.savedraw.TabIndex = 1;
            this.savedraw.Text = "Save";
            this.savedraw.UseVisualStyleBackColor = true;
            this.savedraw.Click += new System.EventHandler(this.savedraw_Click);
            // 
            // black
            // 
            this.black.Appearance = System.Windows.Forms.Appearance.Button;
            this.black.BackColor = System.Drawing.Color.Black;
            this.black.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.black.Location = new System.Drawing.Point(18, 52);
            this.black.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.black.Name = "black";
            this.black.Size = new System.Drawing.Size(82, 92);
            this.black.TabIndex = 2;
            this.black.TabStop = true;
            this.black.UseVisualStyleBackColor = false;
            this.black.CheckedChanged += new System.EventHandler(this.black_CheckedChanged);
            // 
            // white
            // 
            this.white.Appearance = System.Windows.Forms.Appearance.Button;
            this.white.BackColor = System.Drawing.Color.White;
            this.white.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.white.Location = new System.Drawing.Point(18, 150);
            this.white.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.white.Name = "white";
            this.white.Size = new System.Drawing.Size(82, 92);
            this.white.TabIndex = 3;
            this.white.TabStop = true;
            this.white.UseVisualStyleBackColor = false;
            this.white.CheckedChanged += new System.EventHandler(this.white_CheckedChanged);
            // 
            // red
            // 
            this.red.Appearance = System.Windows.Forms.Appearance.Button;
            this.red.BackColor = System.Drawing.Color.Red;
            this.red.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.red.Location = new System.Drawing.Point(18, 248);
            this.red.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.red.Name = "red";
            this.red.Size = new System.Drawing.Size(82, 92);
            this.red.TabIndex = 4;
            this.red.TabStop = true;
            this.red.UseVisualStyleBackColor = false;
            this.red.CheckedChanged += new System.EventHandler(this.red_CheckedChanged);
            // 
            // blue
            // 
            this.blue.Appearance = System.Windows.Forms.Appearance.Button;
            this.blue.BackColor = System.Drawing.Color.Blue;
            this.blue.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.blue.Location = new System.Drawing.Point(18, 346);
            this.blue.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.blue.Name = "blue";
            this.blue.Size = new System.Drawing.Size(82, 92);
            this.blue.TabIndex = 5;
            this.blue.TabStop = true;
            this.blue.UseVisualStyleBackColor = false;
            this.blue.CheckedChanged += new System.EventHandler(this.blue_CheckedChanged);
            // 
            // green
            // 
            this.green.Appearance = System.Windows.Forms.Appearance.Button;
            this.green.BackColor = System.Drawing.Color.Green;
            this.green.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.green.Location = new System.Drawing.Point(18, 444);
            this.green.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.green.Name = "green";
            this.green.Size = new System.Drawing.Size(82, 92);
            this.green.TabIndex = 6;
            this.green.TabStop = true;
            this.green.UseVisualStyleBackColor = false;
            this.green.CheckedChanged += new System.EventHandler(this.green_CheckedChanged);
            // 
            // yellow
            // 
            this.yellow.Appearance = System.Windows.Forms.Appearance.Button;
            this.yellow.BackColor = System.Drawing.Color.Yellow;
            this.yellow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.yellow.Location = new System.Drawing.Point(18, 542);
            this.yellow.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.yellow.Name = "yellow";
            this.yellow.Size = new System.Drawing.Size(82, 92);
            this.yellow.TabIndex = 7;
            this.yellow.TabStop = true;
            this.yellow.UseVisualStyleBackColor = false;
            this.yellow.CheckedChanged += new System.EventHandler(this.yellow_CheckedChanged);
            // 
            // canvas
            // 
            this.canvas.BackColor = System.Drawing.Color.White;
            this.canvas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.canvas.Location = new System.Drawing.Point(117, 52);
            this.canvas.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(500, 500);
            this.canvas.TabIndex = 8;
            this.canvas.TabStop = false;
            this.canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseDown);
            this.canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseMove);
            this.canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseUp);
            // 
            // trackBarThickness
            // 
            this.trackBarThickness.LargeChange = 24;
            this.trackBarThickness.Location = new System.Drawing.Point(117, 589);
            this.trackBarThickness.Maximum = 48;
            this.trackBarThickness.Minimum = 4;
            this.trackBarThickness.Name = "trackBarThickness";
            this.trackBarThickness.Size = new System.Drawing.Size(250, 45);
            this.trackBarThickness.SmallChange = 5;
            this.trackBarThickness.TabIndex = 9;
            this.trackBarThickness.TickFrequency = 4;
            this.trackBarThickness.Value = 8;
            this.trackBarThickness.ValueChanged += new System.EventHandler(this.trackBarThickness_Change);
            // 
            // labPenThickness
            // 
            this.labPenThickness.AutoSize = true;
            this.labPenThickness.Location = new System.Drawing.Point(117, 571);
            this.labPenThickness.Name = "labPenThickness";
            this.labPenThickness.Size = new System.Drawing.Size(50, 15);
            this.labPenThickness.TabIndex = 10;
            this.labPenThickness.Text = "Pen Size";
            // 
            // labPenSize
            // 
            this.labPenSize.AutoSize = true;
            this.labPenSize.Location = new System.Drawing.Point(173, 571);
            this.labPenSize.Name = "labPenSize";
            this.labPenSize.Size = new System.Drawing.Size(19, 15);
            this.labPenSize.TabIndex = 11;
            this.labPenSize.Text = "10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(173, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(326, 21);
            this.label1.TabIndex = 12;
            this.label1.Text = "Be the next Van Gogh; draw your interest!";
            // 
            // PaintForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(651, 661);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labPenSize);
            this.Controls.Add(this.labPenThickness);
            this.Controls.Add(this.trackBarThickness);
            this.Controls.Add(this.canvas);
            this.Controls.Add(this.yellow);
            this.Controls.Add(this.green);
            this.Controls.Add(this.blue);
            this.Controls.Add(this.red);
            this.Controls.Add(this.white);
            this.Controls.Add(this.black);
            this.Controls.Add(this.savedraw);
            this.Controls.Add(this.clearcanvas);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximumSize = new System.Drawing.Size(700, 700);
            this.MinimumSize = new System.Drawing.Size(610, 692);
            this.Name = "PaintForm";
            this.Text = "PaintForm";
            ((System.ComponentModel.ISupportInitialize)(this.canvas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarThickness)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button clearcanvas;
        private System.Windows.Forms.Button savedraw;
        private System.Windows.Forms.RadioButton black;
        private System.Windows.Forms.RadioButton white;
        private System.Windows.Forms.RadioButton red;
        private System.Windows.Forms.RadioButton blue;
        private System.Windows.Forms.RadioButton green;
        private System.Windows.Forms.RadioButton yellow;
        private System.Windows.Forms.PictureBox canvas;
        private TrackBar trackBarThickness;
        private Label labPenThickness;
        private Label labPenSize;
        private Label label1;
    }
}