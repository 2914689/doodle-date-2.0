﻿namespace Dating_App
{
    partial class menuPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menuPage));
            this.menuMenu = new System.Windows.Forms.Button();
            this.menuMatch = new System.Windows.Forms.Button();
            this.menuChat = new System.Windows.Forms.Button();
            this.menuHelp = new System.Windows.Forms.Button();
            this.menuLogin = new System.Windows.Forms.Button();
            this.profilePicture = new System.Windows.Forms.PictureBox();
            this.name = new System.Windows.Forms.Label();
            this.Birthdate = new System.Windows.Forms.Label();
            this.gender = new System.Windows.Forms.Label();
            this.userDrawn = new System.Windows.Forms.GroupBox();
            this.interest0 = new System.Windows.Forms.PictureBox();
            this.interest7 = new System.Windows.Forms.PictureBox();
            this.interest6 = new System.Windows.Forms.PictureBox();
            this.interest5 = new System.Windows.Forms.PictureBox();
            this.interest4 = new System.Windows.Forms.PictureBox();
            this.interest3 = new System.Windows.Forms.PictureBox();
            this.interest2 = new System.Windows.Forms.PictureBox();
            this.interest1 = new System.Windows.Forms.PictureBox();
            this.user = new System.Windows.Forms.Label();
            this.pName = new System.Windows.Forms.Label();
            this.pUsername = new System.Windows.Forms.Label();
            this.pBirthdate = new System.Windows.Forms.Label();
            this.pGender = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.userPFP = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pEmail = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.profilePicture)).BeginInit();
            this.userDrawn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.interest0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.userPFP.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuMenu
            // 
            this.menuMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.menuMenu.BackgroundImage = global::Dating_App.Properties.Resources.homeicon;
            this.menuMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuMenu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuMenu.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown;
            this.menuMenu.FlatAppearance.BorderSize = 3;
            this.menuMenu.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.menuMenu.Location = new System.Drawing.Point(0, 0);
            this.menuMenu.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.menuMenu.Name = "menuMenu";
            this.menuMenu.Size = new System.Drawing.Size(400, 140);
            this.menuMenu.TabIndex = 0;
            this.menuMenu.UseVisualStyleBackColor = false;
            // 
            // menuMatch
            // 
            this.menuMatch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.menuMatch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuMatch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuMatch.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown;
            this.menuMatch.FlatAppearance.BorderSize = 3;
            this.menuMatch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.menuMatch.Image = global::Dating_App.Properties.Resources.matchicon;
            this.menuMatch.Location = new System.Drawing.Point(0, 140);
            this.menuMatch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.menuMatch.Name = "menuMatch";
            this.menuMatch.Size = new System.Drawing.Size(400, 140);
            this.menuMatch.TabIndex = 1;
            this.menuMatch.UseVisualStyleBackColor = false;
            this.menuMatch.Click += new System.EventHandler(this.menuMatch_Click);
            // 
            // menuChat
            // 
            this.menuChat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.menuChat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuChat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuChat.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown;
            this.menuChat.FlatAppearance.BorderSize = 3;
            this.menuChat.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.menuChat.Image = global::Dating_App.Properties.Resources.chaticon;
            this.menuChat.Location = new System.Drawing.Point(0, 280);
            this.menuChat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.menuChat.Name = "menuChat";
            this.menuChat.Size = new System.Drawing.Size(400, 140);
            this.menuChat.TabIndex = 2;
            this.menuChat.UseVisualStyleBackColor = false;
            this.menuChat.Click += new System.EventHandler(this.menuChat_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.menuHelp.BackgroundImage = global::Dating_App.Properties.Resources.helpicon;
            this.menuHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuHelp.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown;
            this.menuHelp.FlatAppearance.BorderSize = 3;
            this.menuHelp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.menuHelp.Location = new System.Drawing.Point(0, 420);
            this.menuHelp.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.menuHelp.Name = "menuHelp";
            this.menuHelp.Size = new System.Drawing.Size(400, 140);
            this.menuHelp.TabIndex = 3;
            this.menuHelp.UseVisualStyleBackColor = false;
            this.menuHelp.Click += new System.EventHandler(this.menuHelp_Click);
            // 
            // menuLogin
            // 
            this.menuLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.menuLogin.BackgroundImage = global::Dating_App.Properties.Resources.logouticon;
            this.menuLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.menuLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuLogin.FlatAppearance.BorderColor = System.Drawing.Color.SaddleBrown;
            this.menuLogin.FlatAppearance.BorderSize = 3;
            this.menuLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.menuLogin.Location = new System.Drawing.Point(0, 560);
            this.menuLogin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.menuLogin.Name = "menuLogin";
            this.menuLogin.Size = new System.Drawing.Size(400, 140);
            this.menuLogin.TabIndex = 4;
            this.menuLogin.UseVisualStyleBackColor = false;
            this.menuLogin.Click += new System.EventHandler(this.menuLogin_Click);
            // 
            // profilePicture
            // 
            this.profilePicture.BackColor = System.Drawing.Color.White;
            this.profilePicture.Location = new System.Drawing.Point(62, 34);
            this.profilePicture.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.profilePicture.Name = "profilePicture";
            this.profilePicture.Size = new System.Drawing.Size(140, 171);
            this.profilePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.profilePicture.TabIndex = 5;
            this.profilePicture.TabStop = false;
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.BackColor = System.Drawing.Color.Transparent;
            this.name.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.name.Location = new System.Drawing.Point(47, 35);
            this.name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(60, 21);
            this.name.TabIndex = 6;
            this.name.Text = "Name: ";
            // 
            // Birthdate
            // 
            this.Birthdate.AutoSize = true;
            this.Birthdate.BackColor = System.Drawing.Color.Transparent;
            this.Birthdate.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Birthdate.Location = new System.Drawing.Point(47, 108);
            this.Birthdate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Birthdate.Name = "Birthdate";
            this.Birthdate.Size = new System.Drawing.Size(82, 21);
            this.Birthdate.TabIndex = 7;
            this.Birthdate.Text = "Birthdate:";
            // 
            // gender
            // 
            this.gender.AutoSize = true;
            this.gender.BackColor = System.Drawing.Color.Transparent;
            this.gender.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.gender.Location = new System.Drawing.Point(48, 145);
            this.gender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.gender.Name = "gender";
            this.gender.Size = new System.Drawing.Size(68, 21);
            this.gender.TabIndex = 8;
            this.gender.Text = "Gender:";
            this.gender.Click += new System.EventHandler(this.gender_Click);
            // 
            // userDrawn
            // 
            this.userDrawn.BackColor = System.Drawing.Color.AliceBlue;
            this.userDrawn.Controls.Add(this.interest0);
            this.userDrawn.Controls.Add(this.interest7);
            this.userDrawn.Controls.Add(this.interest6);
            this.userDrawn.Controls.Add(this.interest5);
            this.userDrawn.Controls.Add(this.interest4);
            this.userDrawn.Controls.Add(this.interest3);
            this.userDrawn.Controls.Add(this.interest2);
            this.userDrawn.Controls.Add(this.interest1);
            this.userDrawn.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.userDrawn.Location = new System.Drawing.Point(443, 98);
            this.userDrawn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.userDrawn.Name = "userDrawn";
            this.userDrawn.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.userDrawn.Size = new System.Drawing.Size(660, 475);
            this.userDrawn.TabIndex = 9;
            this.userDrawn.TabStop = false;
            this.userDrawn.Text = "My Gallery";
            // 
            // interest0
            // 
            this.interest0.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("interest0.BackgroundImage")));
            this.interest0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.interest0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest0.Location = new System.Drawing.Point(19, 45);
            this.interest0.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest0.Name = "interest0";
            this.interest0.Size = new System.Drawing.Size(151, 196);
            this.interest0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest0.TabIndex = 18;
            this.interest0.TabStop = false;
            this.interest0.Tag = "0";
            this.interest0.Click += new System.EventHandler(this.drawingButtonsClick);
            // 
            // interest7
            // 
            this.interest7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("interest7.BackgroundImage")));
            this.interest7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.interest7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest7.Location = new System.Drawing.Point(491, 250);
            this.interest7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest7.Name = "interest7";
            this.interest7.Size = new System.Drawing.Size(151, 196);
            this.interest7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest7.TabIndex = 17;
            this.interest7.TabStop = false;
            this.interest7.Tag = "7";
            this.interest7.Click += new System.EventHandler(this.drawingButtonsClick);
            // 
            // interest6
            // 
            this.interest6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("interest6.BackgroundImage")));
            this.interest6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.interest6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest6.Location = new System.Drawing.Point(334, 250);
            this.interest6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest6.Name = "interest6";
            this.interest6.Size = new System.Drawing.Size(151, 196);
            this.interest6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest6.TabIndex = 16;
            this.interest6.TabStop = false;
            this.interest6.Tag = "6";
            this.interest6.Click += new System.EventHandler(this.drawingButtonsClick);
            // 
            // interest5
            // 
            this.interest5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("interest5.BackgroundImage")));
            this.interest5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.interest5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest5.Location = new System.Drawing.Point(176, 250);
            this.interest5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest5.Name = "interest5";
            this.interest5.Size = new System.Drawing.Size(151, 196);
            this.interest5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest5.TabIndex = 15;
            this.interest5.TabStop = false;
            this.interest5.Tag = "5";
            this.interest5.Click += new System.EventHandler(this.drawingButtonsClick);
            // 
            // interest4
            // 
            this.interest4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("interest4.BackgroundImage")));
            this.interest4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.interest4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest4.Location = new System.Drawing.Point(19, 250);
            this.interest4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest4.Name = "interest4";
            this.interest4.Size = new System.Drawing.Size(151, 196);
            this.interest4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest4.TabIndex = 14;
            this.interest4.TabStop = false;
            this.interest4.Tag = "4";
            this.interest4.Click += new System.EventHandler(this.drawingButtonsClick);
            // 
            // interest3
            // 
            this.interest3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("interest3.BackgroundImage")));
            this.interest3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.interest3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest3.Location = new System.Drawing.Point(491, 45);
            this.interest3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest3.Name = "interest3";
            this.interest3.Size = new System.Drawing.Size(151, 196);
            this.interest3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest3.TabIndex = 13;
            this.interest3.TabStop = false;
            this.interest3.Tag = "3";
            this.interest3.Click += new System.EventHandler(this.drawingButtonsClick);
            // 
            // interest2
            // 
            this.interest2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("interest2.BackgroundImage")));
            this.interest2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.interest2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest2.Location = new System.Drawing.Point(334, 45);
            this.interest2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest2.Name = "interest2";
            this.interest2.Size = new System.Drawing.Size(151, 196);
            this.interest2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest2.TabIndex = 12;
            this.interest2.TabStop = false;
            this.interest2.Tag = "2";
            this.interest2.Click += new System.EventHandler(this.drawingButtonsClick);
            // 
            // interest1
            // 
            this.interest1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("interest1.BackgroundImage")));
            this.interest1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.interest1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest1.Location = new System.Drawing.Point(176, 45);
            this.interest1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest1.Name = "interest1";
            this.interest1.Size = new System.Drawing.Size(151, 196);
            this.interest1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest1.TabIndex = 11;
            this.interest1.TabStop = false;
            this.interest1.Tag = "1";
            this.interest1.Click += new System.EventHandler(this.drawingButtonsClick);
            // 
            // user
            // 
            this.user.AutoSize = true;
            this.user.BackColor = System.Drawing.Color.Transparent;
            this.user.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.user.Location = new System.Drawing.Point(47, 72);
            this.user.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(92, 21);
            this.user.TabIndex = 10;
            this.user.Text = "Username: ";
            // 
            // pName
            // 
            this.pName.AutoSize = true;
            this.pName.BackColor = System.Drawing.Color.Transparent;
            this.pName.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pName.Location = new System.Drawing.Point(148, 35);
            this.pName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pName.Name = "pName";
            this.pName.Size = new System.Drawing.Size(60, 21);
            this.pName.TabIndex = 11;
            this.pName.Text = "Name: ";
            // 
            // pUsername
            // 
            this.pUsername.AutoSize = true;
            this.pUsername.BackColor = System.Drawing.Color.Transparent;
            this.pUsername.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pUsername.Location = new System.Drawing.Point(148, 72);
            this.pUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pUsername.Name = "pUsername";
            this.pUsername.Size = new System.Drawing.Size(92, 21);
            this.pUsername.TabIndex = 12;
            this.pUsername.Text = "Username: ";
            // 
            // pBirthdate
            // 
            this.pBirthdate.AutoSize = true;
            this.pBirthdate.BackColor = System.Drawing.Color.Transparent;
            this.pBirthdate.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pBirthdate.Location = new System.Drawing.Point(148, 108);
            this.pBirthdate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pBirthdate.Name = "pBirthdate";
            this.pBirthdate.Size = new System.Drawing.Size(82, 21);
            this.pBirthdate.TabIndex = 13;
            this.pBirthdate.Text = "Birthdate:";
            // 
            // pGender
            // 
            this.pGender.AutoSize = true;
            this.pGender.BackColor = System.Drawing.Color.Transparent;
            this.pGender.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pGender.Location = new System.Drawing.Point(148, 145);
            this.pGender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pGender.Name = "pGender";
            this.pGender.Size = new System.Drawing.Size(68, 21);
            this.pGender.TabIndex = 14;
            this.pGender.Text = "Gender:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.groupBox1.Controls.Add(this.userPFP);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.userDrawn);
            this.groupBox1.Location = new System.Drawing.Point(538, 130);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1219, 669);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            // 
            // userPFP
            // 
            this.userPFP.BackColor = System.Drawing.Color.AliceBlue;
            this.userPFP.Controls.Add(this.profilePicture);
            this.userPFP.Location = new System.Drawing.Point(109, 98);
            this.userPFP.Name = "userPFP";
            this.userPFP.Size = new System.Drawing.Size(279, 226);
            this.userPFP.TabIndex = 16;
            this.userPFP.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.AliceBlue;
            this.groupBox2.Controls.Add(this.pEmail);
            this.groupBox2.Controls.Add(this.email);
            this.groupBox2.Controls.Add(this.pGender);
            this.groupBox2.Controls.Add(this.gender);
            this.groupBox2.Controls.Add(this.user);
            this.groupBox2.Controls.Add(this.pBirthdate);
            this.groupBox2.Controls.Add(this.Birthdate);
            this.groupBox2.Controls.Add(this.pName);
            this.groupBox2.Controls.Add(this.pUsername);
            this.groupBox2.Controls.Add(this.name);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBox2.Location = new System.Drawing.Point(109, 359);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(279, 214);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "My Profile";
            // 
            // pEmail
            // 
            this.pEmail.AutoSize = true;
            this.pEmail.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pEmail.Location = new System.Drawing.Point(148, 177);
            this.pEmail.Name = "pEmail";
            this.pEmail.Size = new System.Drawing.Size(53, 21);
            this.pEmail.TabIndex = 15;
            this.pEmail.Text = "E-mail";
            // 
            // email
            // 
            this.email.AutoSize = true;
            this.email.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.email.Location = new System.Drawing.Point(48, 177);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(57, 21);
            this.email.TabIndex = 15;
            this.email.Text = "E-mail:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 340);
            this.panel1.TabIndex = 16;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(100, 90);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.menuMenu);
            this.panel2.Controls.Add(this.menuMatch);
            this.panel2.Controls.Add(this.menuLogin);
            this.panel2.Controls.Add(this.menuChat);
            this.panel2.Controls.Add(this.menuHelp);
            this.panel2.Location = new System.Drawing.Point(0, 340);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 740);
            this.panel2.TabIndex = 17;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Dating_App.Properties.Resources.DoodleDateLogo1;
            this.pictureBox2.Location = new System.Drawing.Point(0, -17);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(400, 156);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            // 
            // menuPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "menuPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "menuPage";
            this.Load += new System.EventHandler(this.displayImagesOnLoad);
            ((System.ComponentModel.ISupportInitialize)(this.profilePicture)).EndInit();
            this.userDrawn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.interest0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.userPFP.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button menuMenu;
        private System.Windows.Forms.Button menuMatch;
        private System.Windows.Forms.Button menuChat;
        private System.Windows.Forms.Button menuHelp;
        private System.Windows.Forms.Button menuLogin;
        private System.Windows.Forms.PictureBox profilePicture;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label Birthdate;
        private System.Windows.Forms.Label gender;
        private System.Windows.Forms.Label user;
        private System.Windows.Forms.Label pName;
        private System.Windows.Forms.Label pUsername;
        private System.Windows.Forms.Label pBirthdate;
        private System.Windows.Forms.Label pGender;
        private System.Windows.Forms.GroupBox userDrawn;
        private System.Windows.Forms.PictureBox interest7;
        private System.Windows.Forms.PictureBox interest6;
        private System.Windows.Forms.PictureBox interest5;
        private System.Windows.Forms.PictureBox interest4;
        private System.Windows.Forms.PictureBox interest3;
        private System.Windows.Forms.PictureBox interest2;
        private System.Windows.Forms.PictureBox interest1;
        private System.Windows.Forms.PictureBox interest0;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private Panel panel1;
        private Panel panel2;
        private PictureBox pictureBox1;
        private Label pEmail;
        private Label email;
        private GroupBox userPFP;
        private PictureBox pictureBox2;
    }
}