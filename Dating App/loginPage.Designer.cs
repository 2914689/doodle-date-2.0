﻿namespace Dating_App
{
    partial class loginPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.login = new System.Windows.Forms.Button();
            this.register = new System.Windows.Forms.Button();
            this.logUsername = new System.Windows.Forms.TextBox();
            this.logPassword = new System.Windows.Forms.TextBox();
            this.lUser = new System.Windows.Forms.Label();
            this.lPassword = new System.Windows.Forms.Label();
            this.wrong = new System.Windows.Forms.Label();
            this.empty = new System.Windows.Forms.Label();
            this.show = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // login
            // 
            this.login.BackColor = System.Drawing.Color.Azure;
            this.login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.login.FlatAppearance.BorderColor = System.Drawing.Color.LightSkyBlue;
            this.login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.login.Font = new System.Drawing.Font("Candara Light", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.login.Location = new System.Drawing.Point(1192, 762);
            this.login.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(106, 44);
            this.login.TabIndex = 1;
            this.login.Text = "Login";
            this.login.UseVisualStyleBackColor = false;
            this.login.Click += new System.EventHandler(this.login_Click);
            // 
            // register
            // 
            this.register.BackColor = System.Drawing.Color.Transparent;
            this.register.Cursor = System.Windows.Forms.Cursors.Hand;
            this.register.FlatAppearance.BorderColor = System.Drawing.Color.LightSkyBlue;
            this.register.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.register.Font = new System.Drawing.Font("Candara Light", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.register.Location = new System.Drawing.Point(1324, 762);
            this.register.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(121, 44);
            this.register.TabIndex = 2;
            this.register.Text = "New Here?";
            this.register.UseVisualStyleBackColor = false;
            this.register.Click += new System.EventHandler(this.register_Click);
            // 
            // logUsername
            // 
            this.logUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logUsername.Font = new System.Drawing.Font("Candara Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.logUsername.Location = new System.Drawing.Point(1311, 678);
            this.logUsername.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.logUsername.Name = "logUsername";
            this.logUsername.Size = new System.Drawing.Size(150, 30);
            this.logUsername.TabIndex = 3;
            this.logUsername.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.logUsername_KeyPress);
            // 
            // logPassword
            // 
            this.logPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logPassword.Font = new System.Drawing.Font("Candara Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.logPassword.Location = new System.Drawing.Point(1311, 716);
            this.logPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.logPassword.Name = "logPassword";
            this.logPassword.Size = new System.Drawing.Size(150, 30);
            this.logPassword.TabIndex = 4;
            this.logPassword.UseSystemPasswordChar = true;
            this.logPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.logPassword_KeyPress);
            // 
            // lUser
            // 
            this.lUser.AutoSize = true;
            this.lUser.BackColor = System.Drawing.Color.Transparent;
            this.lUser.Font = new System.Drawing.Font("Candara Light", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lUser.Location = new System.Drawing.Point(1165, 678);
            this.lUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lUser.Name = "lUser";
            this.lUser.Size = new System.Drawing.Size(106, 23);
            this.lUser.TabIndex = 5;
            this.lUser.Text = "Username:";
            // 
            // lPassword
            // 
            this.lPassword.AutoSize = true;
            this.lPassword.BackColor = System.Drawing.Color.Transparent;
            this.lPassword.Font = new System.Drawing.Font("Candara Light", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lPassword.Location = new System.Drawing.Point(1171, 718);
            this.lPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lPassword.Name = "lPassword";
            this.lPassword.Size = new System.Drawing.Size(100, 23);
            this.lPassword.TabIndex = 6;
            this.lPassword.Text = "Password:";
            // 
            // wrong
            // 
            this.wrong.AutoSize = true;
            this.wrong.BackColor = System.Drawing.Color.Transparent;
            this.wrong.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.wrong.Location = new System.Drawing.Point(1187, 822);
            this.wrong.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.wrong.Name = "wrong";
            this.wrong.Size = new System.Drawing.Size(215, 19);
            this.wrong.TabIndex = 7;
            this.wrong.Text = "Invalid Password or Username";
            this.wrong.Visible = false;
            // 
            // empty
            // 
            this.empty.AutoSize = true;
            this.empty.BackColor = System.Drawing.Color.Transparent;
            this.empty.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.empty.Location = new System.Drawing.Point(1136, 844);
            this.empty.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.empty.Name = "empty";
            this.empty.Size = new System.Drawing.Size(309, 19);
            this.empty.TabIndex = 8;
            this.empty.Text = "You need to fill in a Username and Password";
            this.empty.Visible = false;
            // 
            // show
            // 
            this.show.BackColor = System.Drawing.Color.Transparent;
            this.show.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.show.Cursor = System.Windows.Forms.Cursors.Hand;
            this.show.Image = global::Dating_App.Properties.Resources.eyecon;
            this.show.Location = new System.Drawing.Point(1468, 716);
            this.show.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.show.Name = "show";
            this.show.Size = new System.Drawing.Size(31, 34);
            this.show.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.show.TabIndex = 9;
            this.show.TabStop = false;
            this.show.MouseDown += new System.Windows.Forms.MouseEventHandler(this.show_MouseDown);
            this.show.MouseUp += new System.Windows.Forms.MouseEventHandler(this.show_MouseUp);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(672, 1080);
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.label1.Font = new System.Drawing.Font("Segoe UI Black", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(75, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(514, 54);
            this.label1.TabIndex = 12;
            this.label1.Text = "Welcome to DoodleDate!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(1192, 635);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(260, 25);
            this.label2.TabIndex = 13;
            this.label2.Text = " log in, or create an account";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Dating_App.Properties.Resources.DoodleDateLogo1;
            this.pictureBox1.Location = new System.Drawing.Point(923, 242);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(919, 489);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // loginPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.show);
            this.Controls.Add(this.empty);
            this.Controls.Add(this.wrong);
            this.Controls.Add(this.lPassword);
            this.Controls.Add(this.lUser);
            this.Controls.Add(this.logPassword);
            this.Controls.Add(this.logUsername);
            this.Controls.Add(this.register);
            this.Controls.Add(this.login);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "loginPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "loginPage";
            ((System.ComponentModel.ISupportInitialize)(this.show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button login;
        private System.Windows.Forms.Button register;
        private System.Windows.Forms.TextBox logUsername;
        private System.Windows.Forms.TextBox logPassword;
        private System.Windows.Forms.Label lUser;
        private System.Windows.Forms.Label lPassword;
        private System.Windows.Forms.Label wrong;
        private System.Windows.Forms.Label empty;
        private System.Windows.Forms.PictureBox show;
        private PictureBox pictureBox2;
        private Label label1;
        private Label label2;
        private PictureBox pictureBox1;
    }
}

