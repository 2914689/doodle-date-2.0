﻿global using System;
global using System.Drawing;
global using System.IO;
global using System.Linq;
global using System.Text;
global using System.Windows.Forms;
global using System.Collections.Generic;

global using Dating_App.Controllers; //own folders

namespace Dating_App;

internal static class Program
{

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static void Main()
    {
        //Application.EnableVisualStyles();
        Application.SetHighDpiMode(HighDpiMode.SystemAware);
        //Application.SetCompatibleTextRenderingDefault(false);
        Application.Run(new loginPage());
    }
}