﻿#nullable enable
using Dating_App.Properties;

namespace Dating_App;

internal partial class
    registerPage : Form //it may be good to add a second password field to confirm the entered password
{
    //temp profile vars
    private string tUsername;
    private string tPassword;
    private string tFullName;
    private DateOnly tDateOfBirth;
    private string tAgePreference;
    private string tGender;
    private string tOrientation;
    private string[] tUserInterests = { "X", "X", "X", "X", "X", "X", "X", "X" };
    public static Image[] tUserImages = new Image[8];

    //interestvalidation
    private string[] _remainingInterests { get; set; } = Database.allInterests;
    private string? _prevInterest;
    private List<string> _selectedInterests { get; set; } = new ();
    private int _currentInterestsNumber = 1;

    public registerPage()
    {
        InitializeComponent();
        setMyCustomFormat();
    }
    public void setMyCustomFormat()
    {
        // Set the Format type and the CustomFormat string.
        pickerBirthdate.Format = DateTimePickerFormat.Custom;
        pickerBirthdate.CustomFormat = "dd-MM-yyyy";
        pickerBirthdate.MaxDate = Database.AgeClamp;
        pickerBirthdate.Value = Database.AgeClamp;


        foreach (ComboBox cb in interestsBox.Controls) //These are all the interests
        {
            cb.Items.AddRange(_remainingInterests);
        }
    }
        
    private void drawingButtonsClickTemp(object sender, EventArgs e)
    {
        TempPaintForm draw = new TempPaintForm($"{((PictureBox)sender).Tag}");
        draw.ShowDialog();
        Tools.displayImages(drawings, tUserImages);
    }

        
    private void cancel_Click(object sender, EventArgs e) //Returns user to login page.
    {
        this.Hide();
        Form loginPage = new loginPage();
        loginPage.ShowDialog();
        this.Close();
    }

    private void save_Click(object sender, EventArgs e)
    {
        tDateOfBirth = DateOnly.Parse(pickerBirthdate.Text);
        if (checkVar(tUsername, "username"))
        {
            if (warning.Text == "This username already exists.")
            {
                MessageBox.Show("Username is already taken","Error");
            }
            {
                if (checkVar(tPassword, "password") && checkVar(tFullName, "full name") &&
                    checkVar(tGender, "gender") && checkVar(tOrientation, "orientation") &&
                    checkVar(tAgePreference, "age preference"))
                {
                    if (setAndCheckInterests())
                    {
                        StringBuilder sb = new StringBuilder(
                            $@"{tUsername}//{tPassword}//{tFullName}//{tDateOfBirth}//{tAgePreference}//{tGender}//{tOrientation}//");
                        string res = Tools.appendStringArray(sb, tUserInterests);
                        using (StreamWriter wr = File.CreateText($"{Database.UserDataDir}{tUsername}.txt"))
                        {
                            wr.WriteLine(Encryption.Crypt(res, 'E'));
                        }
                        Database.Credentials.Add(tUsername, tPassword); //makes it possible to login after registering
                        saveImagesToDisk();

                        this.Hide();
                        Form loginPage = new loginPage();
                        loginPage.ShowDialog();
                        this.Close();
                    }
                }
            }
        }
    }
        
    void saveImagesToDisk()
    {
        string fileDirName = $@"{Database.UserImagesDir}{tUsername}\";
        if (!Directory.Exists(fileDirName))
        {
            Directory.CreateDirectory(fileDirName);
        }
        for (int i = 0; i < tUserImages.Length; i++)
        {
            if (tUserImages[i] != null)
            {
                tUserImages[i].Save(fileDirName+$"{i}.png", System.Drawing.Imaging.ImageFormat.Png);
            }
        }
    }

    bool checkVar(string var, string about)
    {
        if (var == null)
        {
            MessageBox.Show($"A {about} is required", "Error");
            return false;
        }
        return true;
    }
    bool setAndCheckInterests()
    {
        int i = 0;
        foreach (ComboBox cb in interestsBox.Controls)
        {
            if (cb.SelectedItem != null)
            {
                tUserInterests[int.Parse((string)cb.Tag) - 1] = cb.SelectedItem.ToString();
                i++;
            }
        }
        if (i < 4)
        {
            if (MessageBox.Show(
                    "You have selected fewer than 4 interests, this will cause you to match with fewer people. Are you sure?",
                    "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                return true;
            }
            {
                return false;
            }
        }
        return true;
    }
        
    #region Eventhandlers for var changes
    private void textUser_Changed(object sender, EventArgs e)
    {
        tUsername = textUser.Text;
        if (File.Exists($"{Database.UserDataDir}{tUsername}.txt"))
        {
            warning.Text = "This username already exists.";
        }
        else
        {
            warning.Text = null;
        }
    }
    private void textPassword_Changed(object sender, EventArgs e)
    {
        tPassword = textPassword.Text;
    }
    private void textName_Changed(object sender, EventArgs e)
    {
        tFullName = textName.Text;
    }

    public void genderRadioButton_CheckedChanged(object sender, EventArgs e)
    {
        RadioButton rb = sender as RadioButton; //this checks are redundant in this use case, yet it doesn't hurt much to have them
        if (rb == null)
        {
            MessageBox.Show("Sender is not a RadioButton");
            return;
        }
        if (rb.Checked)
        {
            tGender = rb.Text;
        }
    }

    public void orientationRadioButton_CheckedChanged(object sender, EventArgs e)
    {
        RadioButton rb = sender as RadioButton;
        if (rb == null)
        {
            MessageBox.Show("Sender is not a RadioButton");
            return;
        }

        if (rb.Checked)
        {
            tOrientation = rb.Text;
        }
    }

    public void agepreferenceCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox cb = sender as CheckBox;
        if (cb == null)
        {
            MessageBox.Show("Sender is not a CheckBox");
            return;
        }

        if (cb.CheckState == CheckState.Checked)
        {
            tAgePreference = cb.Text;
        }
        else
        {
            tAgePreference = null;
        }
    }
    private void interestBox_Click(object sender, EventArgs e)
    {
        try
        {
            _prevInterest = ((ComboBox)sender).SelectedItem.ToString(); //nullrefexception is handled yet by default does break the program when thrown. If you get this error just go to Debug->Windows->Exception settings and disable System.NullReferenceException under Common Language Runtime Exceptions
        }
        catch 
        { }
    }

    #endregion
    
    private void interestBox_SelectionChangeCommitted(object sender, EventArgs e)
    {
        ComboBox box = (ComboBox)sender;
        if (box.SelectedItem == "" && _prevInterest != null)
        {
            _selectedInterests.Remove(_prevInterest); //nullrefexception is handled yet by default does break the program when thrown. If you get this error just go to Debug->Windows->Exception settings and disable System.NullReferenceException under Common Language Runtime Exceptions
            //should put _nextInterestsNumber-- here, but that might mean that previously used interestboxes get disabled
            return;
        }
        if (!_selectedInterests.Contains(box.SelectedItem.ToString()))
        {
            _selectedInterests.Add(box.SelectedItem.ToString());
            _currentInterestsNumber++;
            foreach (ComboBox cb in interestsBox.Controls)
            {
                int intTagcb = int.Parse(cb.Tag.ToString());
                if (intTagcb <= _currentInterestsNumber)
                {
                    cb.Enabled = true;
                    //foreach (PictureBox pb in drawings.Controls) //could certainly be more optimsed by taking this foreach out of the other foreach -E
                    //{
                    //    if (int.Parse(pb.Tag.ToString()) < _currentInterestsNumber-1) //TODO: unfinished part which would display the drawing up to the currently available interest
                    //    {
                    //        pb.Image =
                    //
                    //
                    //
                    //
                    //
                    //
                    //
                    //
                    //
                    //
                    //
                    //
                    //
                    // ;
                    //        pb.Enabled = true;
                    //    }
                    //}
                }
                else
                {
                    cb.Enabled = false;
                }
            }
        }
        else
        {
            MessageBox.Show("You have already selected this interest", "Error");
            box.SelectedItem = null;
        }
    }

    private void groupBox4_Enter(object sender, EventArgs e)
    {

    }

    private void panel1_Paint(object sender, PaintEventArgs e)
    {

    }

    private void registerPage_Load(object sender, EventArgs e)
    {

    }

    private void groupBox1_Enter(object sender, EventArgs e)
    {

    }
}