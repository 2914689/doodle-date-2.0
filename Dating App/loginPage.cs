﻿namespace Dating_App;

internal partial class loginPage : Form
{
    public loginPage()
    {
        InitializeComponent();
    }

    #region Login Controls
    private void logUsername_KeyPress(object sender, KeyPressEventArgs e)
    {
        if (e.KeyChar == Convert.ToChar(Keys.Return))
        {
            login_Click(sender, e);
        }
    }
    private void logPassword_KeyPress(object sender, KeyPressEventArgs e) //These 2 methods let you press "Enter" to log in.
    {
        if (e.KeyChar == Convert.ToChar(Keys.Return))
        {
            login_Click(sender, e);
        }
    }

    private void login_Click(object sender, EventArgs e) //Checks if the password matches the username using the dictionary. 
    {
        if (string.IsNullOrEmpty(logUsername.Text) || string.IsNullOrEmpty(logPassword.Text)) { empty.Visible = true; wrong.Visible = false; }
        else
        {
            wrong.Visible = false; empty.Visible = false;
            string username = logUsername.Text;
            Database.initialiseDatabase();
            if (Database.Credentials.ContainsKey(username))
            {
                string password = Database.Credentials[username];
                string input = logPassword.Text;

                if (password == input)
                {
                    this.Hide();
                    Database.CurrentUser = new UserProfile(username);
                    Database.CurrentUserImagesDir = Database.UserImagesDir + $@"{Database.CurrentUser.Username}\";
                    Form menuPage = new menuPage();
                    menuPage.ShowDialog();
                    this.Close();
                }
                else if (password != input) { wrong.Visible = true; }
            }
            else { empty.Visible = false; wrong.Visible = true; }
        }
    }
    #endregion

    private void register_Click(object sender, EventArgs e) //Shows the registration page.
    {
        this.Hide();
        Form registerPage = new registerPage();
        registerPage.ShowDialog();
        this.Close();
    }

    #region Show and hide password.
    private void show_MouseDown(object sender, MouseEventArgs e) { logPassword.UseSystemPasswordChar = false; }
    private void show_MouseUp(object sender, MouseEventArgs e) { logPassword.UseSystemPasswordChar = true; }

    #endregion
}