﻿namespace Dating_App
{
    partial class registerPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.labUser = new System.Windows.Forms.Label();
            this.labPassword = new System.Windows.Forms.Label();
            this.labName = new System.Windows.Forms.Label();
            this.labAge = new System.Windows.Forms.Label();
            this.textUser = new System.Windows.Forms.TextBox();
            this.textName = new System.Windows.Forms.TextBox();
            this.genderBox = new System.Windows.Forms.GroupBox();
            this.r3 = new System.Windows.Forms.RadioButton();
            this.r2 = new System.Windows.Forms.RadioButton();
            this.r1 = new System.Windows.Forms.RadioButton();
            this.orientationBox = new System.Windows.Forms.GroupBox();
            this.r6 = new System.Windows.Forms.RadioButton();
            this.r5 = new System.Windows.Forms.RadioButton();
            this.r4 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.labRequired = new System.Windows.Forms.Label();
            this.interestBox1 = new System.Windows.Forms.ComboBox();
            this.interestBox5 = new System.Windows.Forms.ComboBox();
            this.interestBox2 = new System.Windows.Forms.ComboBox();
            this.interestBox6 = new System.Windows.Forms.ComboBox();
            this.interestBox3 = new System.Windows.Forms.ComboBox();
            this.interestBox7 = new System.Windows.Forms.ComboBox();
            this.interestBox4 = new System.Windows.Forms.ComboBox();
            this.interestBox8 = new System.Windows.Forms.ComboBox();
            this.cancel = new System.Windows.Forms.Button();
            this.save = new System.Windows.Forms.Button();
            this.warning = new System.Windows.Forms.Label();
            this.draw7 = new System.Windows.Forms.PictureBox();
            this.draw6 = new System.Windows.Forms.PictureBox();
            this.draw5 = new System.Windows.Forms.PictureBox();
            this.draw4 = new System.Windows.Forms.PictureBox();
            this.draw3 = new System.Windows.Forms.PictureBox();
            this.draw2 = new System.Windows.Forms.PictureBox();
            this.draw1 = new System.Windows.Forms.PictureBox();
            this.draw0 = new System.Windows.Forms.PictureBox();
            this.AgePreferencebox = new System.Windows.Forms.GroupBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.pickerBirthdate = new System.Windows.Forms.DateTimePicker();
            this.interestsBox = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.drawings = new System.Windows.Forms.GroupBox();
            this.genderBox.SuspendLayout();
            this.orientationBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.draw7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw0)).BeginInit();
            this.AgePreferencebox.SuspendLayout();
            this.interestsBox.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.drawings.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(596, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(649, 54);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tell us something about yourself!";
            // 
            // labUser
            // 
            this.labUser.AutoSize = true;
            this.labUser.BackColor = System.Drawing.Color.Transparent;
            this.labUser.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labUser.Location = new System.Drawing.Point(17, 74);
            this.labUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labUser.Name = "labUser";
            this.labUser.Size = new System.Drawing.Size(60, 14);
            this.labUser.TabIndex = 1;
            this.labUser.Text = "Username";
            // 
            // labPassword
            // 
            this.labPassword.AutoSize = true;
            this.labPassword.BackColor = System.Drawing.Color.Transparent;
            this.labPassword.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labPassword.Location = new System.Drawing.Point(19, 110);
            this.labPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labPassword.Name = "labPassword";
            this.labPassword.Size = new System.Drawing.Size(58, 14);
            this.labPassword.TabIndex = 2;
            this.labPassword.Text = "Password";
            // 
            // labName
            // 
            this.labName.AutoSize = true;
            this.labName.BackColor = System.Drawing.Color.Transparent;
            this.labName.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labName.Location = new System.Drawing.Point(105, 73);
            this.labName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(37, 14);
            this.labName.TabIndex = 3;
            this.labName.Text = "Name";
            // 
            // labAge
            // 
            this.labAge.AutoSize = true;
            this.labAge.BackColor = System.Drawing.Color.Transparent;
            this.labAge.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.labAge.Location = new System.Drawing.Point(105, 106);
            this.labAge.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labAge.Name = "labAge";
            this.labAge.Size = new System.Drawing.Size(56, 14);
            this.labAge.TabIndex = 4;
            this.labAge.Text = "Birthdate";
            // 
            // textUser
            // 
            this.textUser.Location = new System.Drawing.Point(83, 70);
            this.textUser.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textUser.Name = "textUser";
            this.textUser.Size = new System.Drawing.Size(116, 23);
            this.textUser.TabIndex = 6;
            this.textUser.TextChanged += new System.EventHandler(this.textUser_Changed);
            // 
            // textName
            // 
            this.textName.Location = new System.Drawing.Point(176, 69);
            this.textName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textName.Name = "textName";
            this.textName.Size = new System.Drawing.Size(116, 23);
            this.textName.TabIndex = 7;
            this.textName.TextChanged += new System.EventHandler(this.textName_Changed);
            // 
            // genderBox
            // 
            this.genderBox.BackColor = System.Drawing.Color.Transparent;
            this.genderBox.Controls.Add(this.r3);
            this.genderBox.Controls.Add(this.r2);
            this.genderBox.Controls.Add(this.r1);
            this.genderBox.Font = new System.Drawing.Font("Candara", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.genderBox.Location = new System.Drawing.Point(438, 43);
            this.genderBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.genderBox.Name = "genderBox";
            this.genderBox.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.genderBox.Size = new System.Drawing.Size(184, 102);
            this.genderBox.TabIndex = 9;
            this.genderBox.TabStop = false;
            this.genderBox.Text = "Gender:";
            // 
            // r3
            // 
            this.r3.AutoSize = true;
            this.r3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.r3.Location = new System.Drawing.Point(7, 75);
            this.r3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.r3.Name = "r3";
            this.r3.Size = new System.Drawing.Size(86, 19);
            this.r3.TabIndex = 13;
            this.r3.Text = "Non-Binary";
            this.r3.UseVisualStyleBackColor = true;
            this.r3.CheckedChanged += new System.EventHandler(this.genderRadioButton_CheckedChanged);
            // 
            // r2
            // 
            this.r2.AutoSize = true;
            this.r2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.r2.Location = new System.Drawing.Point(7, 48);
            this.r2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.r2.Name = "r2";
            this.r2.Size = new System.Drawing.Size(65, 19);
            this.r2.TabIndex = 12;
            this.r2.Text = "Female";
            this.r2.UseVisualStyleBackColor = true;
            this.r2.CheckedChanged += new System.EventHandler(this.genderRadioButton_CheckedChanged);
            // 
            // r1
            // 
            this.r1.AutoSize = true;
            this.r1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.r1.Location = new System.Drawing.Point(7, 22);
            this.r1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.r1.Name = "r1";
            this.r1.Size = new System.Drawing.Size(52, 19);
            this.r1.TabIndex = 11;
            this.r1.Text = "Male";
            this.r1.UseVisualStyleBackColor = true;
            this.r1.CheckedChanged += new System.EventHandler(this.genderRadioButton_CheckedChanged);
            // 
            // orientationBox
            // 
            this.orientationBox.BackColor = System.Drawing.Color.Transparent;
            this.orientationBox.Controls.Add(this.r6);
            this.orientationBox.Controls.Add(this.r5);
            this.orientationBox.Controls.Add(this.r4);
            this.orientationBox.Font = new System.Drawing.Font("Candara", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.orientationBox.Location = new System.Drawing.Point(438, 151);
            this.orientationBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.orientationBox.Name = "orientationBox";
            this.orientationBox.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.orientationBox.Size = new System.Drawing.Size(402, 102);
            this.orientationBox.TabIndex = 10;
            this.orientationBox.TabStop = false;
            this.orientationBox.Text = "Orientation:";
            // 
            // r6
            // 
            this.r6.AutoSize = true;
            this.r6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.r6.Location = new System.Drawing.Point(7, 76);
            this.r6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.r6.Name = "r6";
            this.r6.Size = new System.Drawing.Size(71, 19);
            this.r6.TabIndex = 16;
            this.r6.Text = "Bisexual";
            this.r6.UseVisualStyleBackColor = true;
            this.r6.CheckedChanged += new System.EventHandler(this.orientationRadioButton_CheckedChanged);
            // 
            // r5
            // 
            this.r5.AutoSize = true;
            this.r5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.r5.Location = new System.Drawing.Point(7, 50);
            this.r5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.r5.Name = "r5";
            this.r5.Size = new System.Drawing.Size(94, 19);
            this.r5.TabIndex = 15;
            this.r5.Text = "Homosexual";
            this.r5.UseVisualStyleBackColor = true;
            this.r5.CheckedChanged += new System.EventHandler(this.orientationRadioButton_CheckedChanged);
            // 
            // r4
            // 
            this.r4.AutoSize = true;
            this.r4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.r4.Location = new System.Drawing.Point(7, 23);
            this.r4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.r4.Name = "r4";
            this.r4.Size = new System.Drawing.Size(100, 19);
            this.r4.TabIndex = 14;
            this.r4.Text = "Heterosexual";
            this.r4.UseVisualStyleBackColor = true;
            this.r4.CheckedChanged += new System.EventHandler(this.orientationRadioButton_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Candara", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(59, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(289, 24);
            this.label2.TabIndex = 11;
            this.label2.Text = "What are some of your interests?";
            // 
            // labRequired
            // 
            this.labRequired.AutoSize = true;
            this.labRequired.BackColor = System.Drawing.Color.Transparent;
            this.labRequired.Font = new System.Drawing.Font("Candara", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            this.labRequired.Location = new System.Drawing.Point(35, 67);
            this.labRequired.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labRequired.Name = "labRequired";
            this.labRequired.Size = new System.Drawing.Size(335, 15);
            this.labRequired.TabIndex = 12;
            this.labRequired.Text = "(Weighted in matching with decreasing order of significance)";
            // 
            // interestBox1
            // 
            this.interestBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.interestBox1.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.interestBox1.FormattingEnabled = true;
            this.interestBox1.Items.AddRange(new object[] {
            ""});
            this.interestBox1.Location = new System.Drawing.Point(35, 21);
            this.interestBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interestBox1.Name = "interestBox1";
            this.interestBox1.Size = new System.Drawing.Size(140, 22);
            this.interestBox1.TabIndex = 13;
            this.interestBox1.Tag = "1";
            this.interestBox1.SelectionChangeCommitted += new System.EventHandler(this.interestBox_SelectionChangeCommitted);
            this.interestBox1.Click += new System.EventHandler(this.interestBox_Click);
            // 
            // interestBox5
            // 
            this.interestBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.interestBox5.Enabled = false;
            this.interestBox5.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.interestBox5.FormattingEnabled = true;
            this.interestBox5.Location = new System.Drawing.Point(225, 21);
            this.interestBox5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interestBox5.Name = "interestBox5";
            this.interestBox5.Size = new System.Drawing.Size(140, 22);
            this.interestBox5.TabIndex = 14;
            this.interestBox5.Tag = "5";
            this.interestBox5.SelectionChangeCommitted += new System.EventHandler(this.interestBox_SelectionChangeCommitted);
            this.interestBox5.Click += new System.EventHandler(this.interestBox_Click);
            // 
            // interestBox2
            // 
            this.interestBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.interestBox2.Enabled = false;
            this.interestBox2.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.interestBox2.FormattingEnabled = true;
            this.interestBox2.Location = new System.Drawing.Point(35, 50);
            this.interestBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interestBox2.Name = "interestBox2";
            this.interestBox2.Size = new System.Drawing.Size(140, 22);
            this.interestBox2.TabIndex = 15;
            this.interestBox2.Tag = "2";
            this.interestBox2.SelectionChangeCommitted += new System.EventHandler(this.interestBox_SelectionChangeCommitted);
            this.interestBox2.Click += new System.EventHandler(this.interestBox_Click);
            // 
            // interestBox6
            // 
            this.interestBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.interestBox6.Enabled = false;
            this.interestBox6.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.interestBox6.FormattingEnabled = true;
            this.interestBox6.Location = new System.Drawing.Point(225, 51);
            this.interestBox6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interestBox6.Name = "interestBox6";
            this.interestBox6.Size = new System.Drawing.Size(140, 22);
            this.interestBox6.TabIndex = 16;
            this.interestBox6.Tag = "6";
            this.interestBox6.SelectionChangeCommitted += new System.EventHandler(this.interestBox_SelectionChangeCommitted);
            this.interestBox6.Click += new System.EventHandler(this.interestBox_Click);
            // 
            // interestBox3
            // 
            this.interestBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.interestBox3.Enabled = false;
            this.interestBox3.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.interestBox3.FormattingEnabled = true;
            this.interestBox3.Location = new System.Drawing.Point(35, 82);
            this.interestBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interestBox3.Name = "interestBox3";
            this.interestBox3.Size = new System.Drawing.Size(140, 22);
            this.interestBox3.TabIndex = 17;
            this.interestBox3.Tag = "3";
            this.interestBox3.SelectionChangeCommitted += new System.EventHandler(this.interestBox_SelectionChangeCommitted);
            this.interestBox3.Click += new System.EventHandler(this.interestBox_Click);
            // 
            // interestBox7
            // 
            this.interestBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.interestBox7.Enabled = false;
            this.interestBox7.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.interestBox7.FormattingEnabled = true;
            this.interestBox7.Location = new System.Drawing.Point(225, 82);
            this.interestBox7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interestBox7.Name = "interestBox7";
            this.interestBox7.Size = new System.Drawing.Size(140, 22);
            this.interestBox7.TabIndex = 18;
            this.interestBox7.Tag = "7";
            this.interestBox7.SelectionChangeCommitted += new System.EventHandler(this.interestBox_SelectionChangeCommitted);
            this.interestBox7.Click += new System.EventHandler(this.interestBox_Click);
            // 
            // interestBox4
            // 
            this.interestBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.interestBox4.Enabled = false;
            this.interestBox4.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.interestBox4.FormattingEnabled = true;
            this.interestBox4.Location = new System.Drawing.Point(35, 113);
            this.interestBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interestBox4.Name = "interestBox4";
            this.interestBox4.Size = new System.Drawing.Size(140, 22);
            this.interestBox4.TabIndex = 19;
            this.interestBox4.Tag = "4";
            this.interestBox4.SelectionChangeCommitted += new System.EventHandler(this.interestBox_SelectionChangeCommitted);
            this.interestBox4.Click += new System.EventHandler(this.interestBox_Click);
            // 
            // interestBox8
            // 
            this.interestBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.interestBox8.Enabled = false;
            this.interestBox8.Font = new System.Drawing.Font("Candara", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.interestBox8.FormattingEnabled = true;
            this.interestBox8.Location = new System.Drawing.Point(225, 113);
            this.interestBox8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interestBox8.Name = "interestBox8";
            this.interestBox8.Size = new System.Drawing.Size(140, 22);
            this.interestBox8.TabIndex = 20;
            this.interestBox8.Tag = "8";
            this.interestBox8.SelectionChangeCommitted += new System.EventHandler(this.interestBox_SelectionChangeCommitted);
            this.interestBox8.Click += new System.EventHandler(this.interestBox_Click);
            // 
            // cancel
            // 
            this.cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancel.Font = new System.Drawing.Font("Candara", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.cancel.Location = new System.Drawing.Point(851, 924);
            this.cancel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(97, 46);
            this.cancel.TabIndex = 22;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // save
            // 
            this.save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.save.Font = new System.Drawing.Font("Candara", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.save.Location = new System.Drawing.Point(954, 924);
            this.save.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(97, 46);
            this.save.TabIndex = 23;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // warning
            // 
            this.warning.AutoSize = true;
            this.warning.Location = new System.Drawing.Point(220, 70);
            this.warning.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.warning.Name = "warning";
            this.warning.Size = new System.Drawing.Size(0, 15);
            this.warning.TabIndex = 14;
            // 
            // draw7
            // 
            this.draw7.BackgroundImage = global::Dating_App.Properties.Resources.Default;
            this.draw7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.draw7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.draw7.Cursor = System.Windows.Forms.Cursors.Cross;
            this.draw7.Location = new System.Drawing.Point(212, 709);
            this.draw7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.draw7.Name = "draw7";
            this.draw7.Size = new System.Drawing.Size(190, 190);
            this.draw7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.draw7.TabIndex = 17;
            this.draw7.TabStop = false;
            this.draw7.Tag = "8";
            this.draw7.Click += new System.EventHandler(this.drawingButtonsClickTemp);
            // 
            // draw6
            // 
            this.draw6.BackgroundImage = global::Dating_App.Properties.Resources.Default;
            this.draw6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.draw6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.draw6.Cursor = System.Windows.Forms.Cursors.Cross;
            this.draw6.Location = new System.Drawing.Point(6, 709);
            this.draw6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.draw6.Name = "draw6";
            this.draw6.Size = new System.Drawing.Size(190, 190);
            this.draw6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.draw6.TabIndex = 16;
            this.draw6.TabStop = false;
            this.draw6.Tag = "4";
            this.draw6.Click += new System.EventHandler(this.drawingButtonsClickTemp);
            // 
            // draw5
            // 
            this.draw5.BackgroundImage = global::Dating_App.Properties.Resources.Default;
            this.draw5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.draw5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.draw5.Cursor = System.Windows.Forms.Cursors.Cross;
            this.draw5.Location = new System.Drawing.Point(212, 513);
            this.draw5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.draw5.Name = "draw5";
            this.draw5.Size = new System.Drawing.Size(190, 190);
            this.draw5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.draw5.TabIndex = 15;
            this.draw5.TabStop = false;
            this.draw5.Tag = "7";
            this.draw5.Click += new System.EventHandler(this.drawingButtonsClickTemp);
            // 
            // draw4
            // 
            this.draw4.BackgroundImage = global::Dating_App.Properties.Resources.Default;
            this.draw4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.draw4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.draw4.Cursor = System.Windows.Forms.Cursors.Cross;
            this.draw4.Location = new System.Drawing.Point(7, 513);
            this.draw4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.draw4.Name = "draw4";
            this.draw4.Size = new System.Drawing.Size(190, 190);
            this.draw4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.draw4.TabIndex = 14;
            this.draw4.TabStop = false;
            this.draw4.Tag = "3";
            this.draw4.Click += new System.EventHandler(this.drawingButtonsClickTemp);
            // 
            // draw3
            // 
            this.draw3.BackgroundImage = global::Dating_App.Properties.Resources.Default;
            this.draw3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.draw3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.draw3.Cursor = System.Windows.Forms.Cursors.Cross;
            this.draw3.Location = new System.Drawing.Point(212, 317);
            this.draw3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.draw3.Name = "draw3";
            this.draw3.Size = new System.Drawing.Size(190, 190);
            this.draw3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.draw3.TabIndex = 13;
            this.draw3.TabStop = false;
            this.draw3.Tag = "6";
            this.draw3.Click += new System.EventHandler(this.drawingButtonsClickTemp);
            // 
            // draw2
            // 
            this.draw2.BackgroundImage = global::Dating_App.Properties.Resources.Default;
            this.draw2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.draw2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.draw2.Cursor = System.Windows.Forms.Cursors.Cross;
            this.draw2.Location = new System.Drawing.Point(7, 317);
            this.draw2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.draw2.Name = "draw2";
            this.draw2.Size = new System.Drawing.Size(190, 190);
            this.draw2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.draw2.TabIndex = 12;
            this.draw2.TabStop = false;
            this.draw2.Tag = "2";
            this.draw2.Click += new System.EventHandler(this.drawingButtonsClickTemp);
            // 
            // draw1
            // 
            this.draw1.BackgroundImage = global::Dating_App.Properties.Resources.Default;
            this.draw1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.draw1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.draw1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.draw1.Location = new System.Drawing.Point(212, 121);
            this.draw1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.draw1.Name = "draw1";
            this.draw1.Size = new System.Drawing.Size(190, 190);
            this.draw1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.draw1.TabIndex = 11;
            this.draw1.TabStop = false;
            this.draw1.Tag = "5";
            this.draw1.Click += new System.EventHandler(this.drawingButtonsClickTemp);
            // 
            // draw0
            // 
            this.draw0.BackgroundImage = global::Dating_App.Properties.Resources.Default;
            this.draw0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.draw0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.draw0.Cursor = System.Windows.Forms.Cursors.Cross;
            this.draw0.Location = new System.Drawing.Point(7, 121);
            this.draw0.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.draw0.Name = "draw0";
            this.draw0.Size = new System.Drawing.Size(190, 190);
            this.draw0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.draw0.TabIndex = 10;
            this.draw0.TabStop = false;
            this.draw0.Tag = "1";
            this.draw0.Click += new System.EventHandler(this.drawingButtonsClickTemp);
            // 
            // AgePreferencebox
            // 
            this.AgePreferencebox.BackColor = System.Drawing.Color.Transparent;
            this.AgePreferencebox.Controls.Add(this.checkBox6);
            this.AgePreferencebox.Controls.Add(this.checkBox5);
            this.AgePreferencebox.Controls.Add(this.checkBox4);
            this.AgePreferencebox.Controls.Add(this.checkBox3);
            this.AgePreferencebox.Controls.Add(this.checkBox2);
            this.AgePreferencebox.Controls.Add(this.checkBox1);
            this.AgePreferencebox.Font = new System.Drawing.Font("Candara", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.AgePreferencebox.Location = new System.Drawing.Point(656, 43);
            this.AgePreferencebox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.AgePreferencebox.Name = "AgePreferencebox";
            this.AgePreferencebox.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.AgePreferencebox.Size = new System.Drawing.Size(184, 102);
            this.AgePreferencebox.TabIndex = 26;
            this.AgePreferencebox.TabStop = false;
            this.AgePreferencebox.Text = "Age Preference";
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(90, 75);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(62, 19);
            this.checkBox6.TabIndex = 19;
            this.checkBox6.Text = "43-50+";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.agepreferenceCheckBox_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(90, 46);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(55, 19);
            this.checkBox5.TabIndex = 18;
            this.checkBox5.Text = "38-42";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.agepreferenceCheckBox_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(90, 20);
            this.checkBox4.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(53, 19);
            this.checkBox4.TabIndex = 17;
            this.checkBox4.Text = "33-37";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.agepreferenceCheckBox_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(6, 75);
            this.checkBox3.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(54, 19);
            this.checkBox3.TabIndex = 16;
            this.checkBox3.Text = "28-32";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.agepreferenceCheckBox_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(6, 46);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(53, 19);
            this.checkBox2.TabIndex = 15;
            this.checkBox2.Text = "23-27";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.agepreferenceCheckBox_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 20);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(52, 19);
            this.checkBox1.TabIndex = 14;
            this.checkBox1.Text = "18-22";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.agepreferenceCheckBox_CheckedChanged);
            // 
            // pickerBirthdate
            // 
            this.pickerBirthdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.pickerBirthdate.Location = new System.Drawing.Point(176, 103);
            this.pickerBirthdate.Margin = new System.Windows.Forms.Padding(2);
            this.pickerBirthdate.Name = "pickerBirthdate";
            this.pickerBirthdate.Size = new System.Drawing.Size(116, 23);
            this.pickerBirthdate.TabIndex = 27;
            this.pickerBirthdate.Value = new System.DateTime(2005, 1, 6, 0, 0, 0, 0);
            // 
            // interestsBox
            // 
            this.interestsBox.BackColor = System.Drawing.Color.Transparent;
            this.interestsBox.Controls.Add(this.interestBox5);
            this.interestsBox.Controls.Add(this.interestBox1);
            this.interestsBox.Controls.Add(this.interestBox2);
            this.interestsBox.Controls.Add(this.interestBox6);
            this.interestsBox.Controls.Add(this.interestBox3);
            this.interestsBox.Controls.Add(this.interestBox7);
            this.interestsBox.Controls.Add(this.interestBox4);
            this.interestsBox.Controls.Add(this.interestBox8);
            this.interestsBox.Location = new System.Drawing.Point(24, 95);
            this.interestsBox.Margin = new System.Windows.Forms.Padding(2);
            this.interestsBox.Name = "interestsBox";
            this.interestsBox.Padding = new System.Windows.Forms.Padding(2);
            this.interestsBox.Size = new System.Drawing.Size(371, 151);
            this.interestsBox.TabIndex = 36;
            this.interestsBox.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(902, 520);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 15);
            this.label11.TabIndex = 18;
            this.label11.Text = "1-2-3-4";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(902, 540);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 15);
            this.label12.TabIndex = 19;
            this.label12.Text = "5-6-7-8";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(812, 520);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 15);
            this.label13.TabIndex = 37;
            this.label13.Text = "Interests Order:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(431, 529);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 15);
            this.label14.TabIndex = 40;
            this.label14.Text = "Interests Order:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(494, 547);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(24, 15);
            this.label15.TabIndex = 38;
            this.label15.Text = "1-5";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(494, 567);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(24, 15);
            this.label16.TabIndex = 39;
            this.label16.Text = "2-6";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(494, 585);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 15);
            this.label18.TabIndex = 41;
            this.label18.Text = "3-7";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(494, 605);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(24, 15);
            this.label19.TabIndex = 42;
            this.label19.Text = "4-8";
            // 
            // textPassword
            // 
            this.textPassword.Location = new System.Drawing.Point(83, 103);
            this.textPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textPassword.Name = "textPassword";
            this.textPassword.Size = new System.Drawing.Size(116, 23);
            this.textPassword.TabIndex = 6;
            this.textPassword.TextChanged += new System.EventHandler(this.textPassword_Changed);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(405, 1080);
            this.panel1.TabIndex = 43;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(74, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(250, 37);
            this.label3.TabIndex = 18;
            this.label3.Text = "Draw your Gallery";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 1080);
            this.panel2.TabIndex = 44;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textUser);
            this.groupBox1.Controls.Add(this.labUser);
            this.groupBox1.Controls.Add(this.labPassword);
            this.groupBox1.Controls.Add(this.textPassword);
            this.groupBox1.Controls.Add(this.warning);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBox1.Location = new System.Drawing.Point(531, 264);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 200);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Account Information";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(47, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(235, 15);
            this.label6.TabIndex = 16;
            this.label6.Text = "(be sure not to share these with anyone!)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(24, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(272, 21);
            this.label5.TabIndex = 15;
            this.label5.Text = "Your (public) username and password";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.labName);
            this.groupBox2.Controls.Add(this.labAge);
            this.groupBox2.Controls.Add(this.textName);
            this.groupBox2.Controls.Add(this.pickerBirthdate);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBox2.Location = new System.Drawing.Point(1011, 264);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(400, 200);
            this.groupBox2.TabIndex = 46;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Personal Information";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(91, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(201, 15);
            this.label7.TabIndex = 28;
            this.label7.Text = "You won\'t be able to change these!";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.labRequired);
            this.groupBox3.Controls.Add(this.interestsBox);
            this.groupBox3.Controls.Add(this.genderBox);
            this.groupBox3.Controls.Add(this.AgePreferencebox);
            this.groupBox3.Controls.Add(this.orientationBox);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.groupBox3.Location = new System.Drawing.Point(531, 531);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(880, 278);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "About You";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(671, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(511, 25);
            this.label4.TabIndex = 48;
            this.label4.Text = "Here\'s where you enter all the information for your account";
            // 
            // drawings
            // 
            this.drawings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.drawings.Controls.Add(this.label3);
            this.drawings.Controls.Add(this.draw0);
            this.drawings.Controls.Add(this.draw7);
            this.drawings.Controls.Add(this.draw3);
            this.drawings.Controls.Add(this.draw4);
            this.drawings.Controls.Add(this.draw6);
            this.drawings.Controls.Add(this.draw2);
            this.drawings.Controls.Add(this.draw5);
            this.drawings.Controls.Add(this.draw1);
            this.drawings.Location = new System.Drawing.Point(1500, -13);
            this.drawings.Name = "drawings";
            this.drawings.Size = new System.Drawing.Size(405, 1093);
            this.drawings.TabIndex = 49;
            this.drawings.TabStop = false;
            this.drawings.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // registerPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.save);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.drawings);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "registerPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "registerPage";
            this.Load += new System.EventHandler(this.registerPage_Load);
            this.genderBox.ResumeLayout(false);
            this.genderBox.PerformLayout();
            this.orientationBox.ResumeLayout(false);
            this.orientationBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.draw7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.draw0)).EndInit();
            this.AgePreferencebox.ResumeLayout(false);
            this.AgePreferencebox.PerformLayout();
            this.interestsBox.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.drawings.ResumeLayout(false);
            this.drawings.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labUser;
        private System.Windows.Forms.Label labPassword;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label labAge;
        private System.Windows.Forms.TextBox textUser;
        private System.Windows.Forms.TextBox textPassword;
        private System.Windows.Forms.TextBox textName;
        private System.Windows.Forms.GroupBox genderBox;
        private System.Windows.Forms.RadioButton r3;
        private System.Windows.Forms.RadioButton r2;
        private System.Windows.Forms.RadioButton r1;
        private System.Windows.Forms.GroupBox orientationBox;
        private System.Windows.Forms.RadioButton r6;
        private System.Windows.Forms.RadioButton r5;
        private System.Windows.Forms.RadioButton r4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labRequired;
        private System.Windows.Forms.ComboBox interestBox1;
        private System.Windows.Forms.ComboBox interestBox5;
        private System.Windows.Forms.ComboBox interestBox2;
        private System.Windows.Forms.ComboBox interestBox6;
        private System.Windows.Forms.ComboBox interestBox3;
        private System.Windows.Forms.ComboBox interestBox7;
        private System.Windows.Forms.ComboBox interestBox4;
        private System.Windows.Forms.ComboBox interestBox8;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label warning;
        private System.Windows.Forms.PictureBox draw7;
        private System.Windows.Forms.PictureBox draw6;
        private System.Windows.Forms.PictureBox draw5;
        private System.Windows.Forms.PictureBox draw4;
        private System.Windows.Forms.PictureBox draw3;
        private System.Windows.Forms.PictureBox draw2;
        private System.Windows.Forms.PictureBox draw1;
        private System.Windows.Forms.PictureBox draw0;
        private System.Windows.Forms.GroupBox AgePreferencebox;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DateTimePicker pickerBirthdate;
        private System.Windows.Forms.GroupBox interestsBox;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private Label label19;
        private Panel panel1;
        private Label label3;
        private Panel panel2;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private GroupBox groupBox3;
        private Label label6;
        private Label label5;
        private Label label7;
        private Label label4;
        private GroupBox drawings;
        private PictureBox pictureBox1;
    }
}