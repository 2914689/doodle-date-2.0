﻿namespace Dating_App
{
    partial class chatPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chatLogin = new System.Windows.Forms.Button();
            this.chatHelp = new System.Windows.Forms.Button();
            this.chatChat = new System.Windows.Forms.Button();
            this.chatMatch = new System.Windows.Forms.Button();
            this.chatHome = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.msgList = new System.Windows.Forms.ListBox();
            this.msgBox = new System.Windows.Forms.TextBox();
            this.send = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chat = new System.Windows.Forms.Label();
            this.manual = new System.Windows.Forms.LinkLabel();
            this.NetTool = new System.Windows.Forms.GroupBox();
            this.OK = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PR = new System.Windows.Forms.TextBox();
            this.IPR = new System.Windows.Forms.TextBox();
            this.PL = new System.Windows.Forms.TextBox();
            this.IPL = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupContacts = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.NetTool.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // chatLogin
            // 
            this.chatLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.chatLogin.BackgroundImage = global::Dating_App.Properties.Resources.logouticon;
            this.chatLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chatLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chatLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chatLogin.Location = new System.Drawing.Point(0, 560);
            this.chatLogin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chatLogin.Name = "chatLogin";
            this.chatLogin.Size = new System.Drawing.Size(400, 140);
            this.chatLogin.TabIndex = 9;
            this.chatLogin.UseVisualStyleBackColor = false;
            this.chatLogin.Click += new System.EventHandler(this.chatLogin_Click);
            // 
            // chatHelp
            // 
            this.chatHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.chatHelp.BackgroundImage = global::Dating_App.Properties.Resources.helpicon;
            this.chatHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chatHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chatHelp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chatHelp.Location = new System.Drawing.Point(0, 420);
            this.chatHelp.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chatHelp.Name = "chatHelp";
            this.chatHelp.Size = new System.Drawing.Size(400, 140);
            this.chatHelp.TabIndex = 8;
            this.chatHelp.UseVisualStyleBackColor = false;
            this.chatHelp.Click += new System.EventHandler(this.chatHelp_Click);
            // 
            // chatChat
            // 
            this.chatChat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.chatChat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chatChat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chatChat.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chatChat.Image = global::Dating_App.Properties.Resources.chaticon;
            this.chatChat.Location = new System.Drawing.Point(0, 280);
            this.chatChat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chatChat.Name = "chatChat";
            this.chatChat.Size = new System.Drawing.Size(400, 140);
            this.chatChat.TabIndex = 7;
            this.chatChat.UseVisualStyleBackColor = false;
            // 
            // chatMatch
            // 
            this.chatMatch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.chatMatch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chatMatch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chatMatch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chatMatch.Image = global::Dating_App.Properties.Resources.matchicon;
            this.chatMatch.Location = new System.Drawing.Point(0, 140);
            this.chatMatch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chatMatch.Name = "chatMatch";
            this.chatMatch.Size = new System.Drawing.Size(400, 140);
            this.chatMatch.TabIndex = 6;
            this.chatMatch.UseVisualStyleBackColor = false;
            this.chatMatch.Click += new System.EventHandler(this.chatMatch_Click);
            // 
            // chatHome
            // 
            this.chatHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.chatHome.BackgroundImage = global::Dating_App.Properties.Resources.homeicon;
            this.chatHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.chatHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chatHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chatHome.Location = new System.Drawing.Point(0, 0);
            this.chatHome.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.chatHome.Name = "chatHome";
            this.chatHome.Size = new System.Drawing.Size(400, 140);
            this.chatHome.TabIndex = 5;
            this.chatHome.UseVisualStyleBackColor = false;
            this.chatHome.Click += new System.EventHandler(this.chatHome_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Candara", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(51, 137);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 33);
            this.label1.TabIndex = 13;
            this.label1.Text = "Open Chats";
            // 
            // msgList
            // 
            this.msgList.BackColor = System.Drawing.Color.AliceBlue;
            this.msgList.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.msgList.Font = new System.Drawing.Font("Candara", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.msgList.FormattingEnabled = true;
            this.msgList.ItemHeight = 23;
            this.msgList.Location = new System.Drawing.Point(327, 148);
            this.msgList.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.msgList.Name = "msgList";
            this.msgList.Size = new System.Drawing.Size(696, 349);
            this.msgList.TabIndex = 16;
            // 
            // msgBox
            // 
            this.msgBox.AcceptsTab = true;
            this.msgBox.BackColor = System.Drawing.Color.AliceBlue;
            this.msgBox.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.msgBox.Location = new System.Drawing.Point(327, 506);
            this.msgBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.msgBox.MaxLength = 200;
            this.msgBox.Multiline = true;
            this.msgBox.Name = "msgBox";
            this.msgBox.Size = new System.Drawing.Size(602, 110);
            this.msgBox.TabIndex = 17;
            this.msgBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.message_KeyPress);
            // 
            // send
            // 
            this.send.BackColor = System.Drawing.Color.AliceBlue;
            this.send.Cursor = System.Windows.Forms.Cursors.Hand;
            this.send.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.send.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.send.Font = new System.Drawing.Font("Candara", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.send.Location = new System.Drawing.Point(937, 505);
            this.send.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(88, 112);
            this.send.TabIndex = 18;
            this.send.Text = "Send";
            this.send.UseVisualStyleBackColor = false;
            this.send.Click += new System.EventHandler(this.send_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(327, 42);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // chat
            // 
            this.chat.AutoSize = true;
            this.chat.BackColor = System.Drawing.Color.Transparent;
            this.chat.Font = new System.Drawing.Font("Candara Light", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.chat.Location = new System.Drawing.Point(1230, 304);
            this.chat.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.chat.Name = "chat";
            this.chat.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chat.Size = new System.Drawing.Size(0, 36);
            this.chat.TabIndex = 20;
            // 
            // manual
            // 
            this.manual.AutoSize = true;
            this.manual.BackColor = System.Drawing.Color.Transparent;
            this.manual.Location = new System.Drawing.Point(971, 127);
            this.manual.Name = "manual";
            this.manual.Size = new System.Drawing.Size(48, 15);
            this.manual.TabIndex = 22;
            this.manual.TabStop = true;
            this.manual.Text = "NetTool";
            this.manual.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.manual_LinkClicked);
            // 
            // NetTool
            // 
            this.NetTool.Controls.Add(this.OK);
            this.NetTool.Controls.Add(this.label4);
            this.NetTool.Controls.Add(this.label3);
            this.NetTool.Controls.Add(this.label2);
            this.NetTool.Controls.Add(this.label5);
            this.NetTool.Controls.Add(this.PR);
            this.NetTool.Controls.Add(this.IPR);
            this.NetTool.Controls.Add(this.PL);
            this.NetTool.Controls.Add(this.IPL);
            this.NetTool.Enabled = false;
            this.NetTool.Location = new System.Drawing.Point(560, 247);
            this.NetTool.Name = "NetTool";
            this.NetTool.Size = new System.Drawing.Size(230, 160);
            this.NetTool.TabIndex = 23;
            this.NetTool.TabStop = false;
            this.NetTool.Text = "Network Tool";
            this.NetTool.Visible = false;
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(75, 122);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 26;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(122, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 15);
            this.label4.TabIndex = 25;
            this.label4.Text = "Remote Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(122, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 15);
            this.label3.TabIndex = 24;
            this.label3.Text = "Local Port";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 15);
            this.label2.TabIndex = 23;
            this.label2.Text = "Remote IP";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 15);
            this.label5.TabIndex = 22;
            this.label5.Text = "Local IP";
            // 
            // PR
            // 
            this.PR.Location = new System.Drawing.Point(122, 93);
            this.PR.Name = "PR";
            this.PR.Size = new System.Drawing.Size(100, 23);
            this.PR.TabIndex = 21;
            // 
            // IPR
            // 
            this.IPR.Location = new System.Drawing.Point(6, 93);
            this.IPR.Name = "IPR";
            this.IPR.Size = new System.Drawing.Size(100, 23);
            this.IPR.TabIndex = 20;
            // 
            // PL
            // 
            this.PL.Location = new System.Drawing.Point(122, 45);
            this.PL.Name = "PL";
            this.PL.Size = new System.Drawing.Size(100, 23);
            this.PL.TabIndex = 19;
            // 
            // IPL
            // 
            this.IPL.Location = new System.Drawing.Point(6, 45);
            this.IPL.Name = "IPL";
            this.IPL.Size = new System.Drawing.Size(100, 23);
            this.IPL.TabIndex = 18;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 340);
            this.panel1.TabIndex = 24;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(100, 99);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(200, 200);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Dating_App.Properties.Resources.DoodleDateLogo1;
            this.pictureBox2.Location = new System.Drawing.Point(0, -17);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(400, 156);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.panel2.Controls.Add(this.chatHome);
            this.panel2.Controls.Add(this.chatMatch);
            this.panel2.Controls.Add(this.chatChat);
            this.panel2.Controls.Add(this.chatHelp);
            this.panel2.Controls.Add(this.chatLogin);
            this.panel2.Location = new System.Drawing.Point(0, 340);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 740);
            this.panel2.TabIndex = 25;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.panel3.Controls.Add(this.NetTool);
            this.panel3.Controls.Add(this.manual);
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.send);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.msgBox);
            this.panel3.Controls.Add(this.msgList);
            this.panel3.Controls.Add(this.groupContacts);
            this.panel3.Location = new System.Drawing.Point(573, 234);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1061, 666);
            this.panel3.TabIndex = 26;
            // 
            // groupContacts
            // 
            this.groupContacts.BackColor = System.Drawing.Color.White;
            this.groupContacts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.groupContacts.Location = new System.Drawing.Point(25, 185);
            this.groupContacts.Name = "groupContacts";
            this.groupContacts.Size = new System.Drawing.Size(215, 432);
            this.groupContacts.TabIndex = 27;
            this.groupContacts.TabStop = false;
            // 
            // chatPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chat);
            this.Controls.Add(this.panel3);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "chatPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "chatPage";
            this.Load += new System.EventHandler(this.chatPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.NetTool.ResumeLayout(false);
            this.NetTool.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button chatLogin;
        private System.Windows.Forms.Button chatHelp;
        private System.Windows.Forms.Button chatChat;
        private System.Windows.Forms.Button chatMatch;
        private System.Windows.Forms.Button chatHome;
        private System.Windows.Forms.RadioButton contact0;
        private System.Windows.Forms.RadioButton contact1;
        private System.Windows.Forms.RadioButton contact2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton contact3;
        private System.Windows.Forms.RadioButton contact4;
        private System.Windows.Forms.TextBox msgBox;
        private System.Windows.Forms.Button send;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label chat;
        private System.Windows.Forms.RadioButton contact5;
        public ListBox msgList;
        private LinkLabel manual;
        private GroupBox NetTool;
        private Button OK;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label5;
        private TextBox PR;
        private TextBox IPR;
        private TextBox PL;
        private TextBox IPL;
        private Panel panel1;
        private Panel panel2;
        private Panel panel3;
        private PictureBox pictureBox3;
        private Panel panel4;
        private PictureBox pictureBox2;
        private GroupBox groupContacts;
    }
}