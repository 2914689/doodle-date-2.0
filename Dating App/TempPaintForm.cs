﻿namespace Dating_App;

internal class TempPaintForm : PaintForm
{
    public TempPaintForm(string n) : base (n)
    {

    }
    public override void savedraw_Click(object sender, EventArgs e) //Saves the drawing to the user's folder.
    {
        registerPage.tUserImages[int.Parse(_interest)] = bmp;
        Close();
    }
}