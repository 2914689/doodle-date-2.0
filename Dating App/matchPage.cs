﻿namespace Dating_App;

internal partial class matchPage : Form
{
    private bool firstTime = true;
    public matchPage()
    {
        Matching.makeCurrentMatch();//wordt twee keer geladen  door matchpage_load
        InitializeComponent();
        displayProfile(Matching.CurrentMatch);
    }
    void displayProfile(UserProfile u)
    {
        pName.Text = u.FullName;
        pBirthdate.Text = u.DateOfBirth.ToString();
        pGender.Text = u.Gender;
        displayImages(u);
        displayProfilePicture(u);
        Tools.displayPFP(pictureBox1);
    }
    void displayProfilePicture(UserProfile u)
    {
        foreach(PictureBox pb in userPFP.Controls)
        {
            if (u.UserImages[0] != null)
            {
                pb.Image = u.UserImages[0];
            }
        }

    }
    private void matchPage_Load(object sender, EventArgs e)
    {
        if (!firstTime)
        {
            Matching.makeCurrentMatch();
            displayProfile(Matching.CurrentMatch);
        }
        firstTime = false;
    }

    void displayImages(UserProfile u)
    {
        foreach (PictureBox pb in userDrawn.Controls)
        {
            if (u.UserImages[int.Parse(pb.Tag.ToString())] != null)
            {
                pb.Image = u.UserImages[(int.Parse(pb.Tag.ToString()))];
            }
            else
            {
                pb.Image = Properties.Resources.emptyCanvas;
            }
        }
    }
    private void makeMatch_Click(object sender, EventArgs e)
    {
        Matching.makeCurrentMatch();
        displayProfile(Matching.CurrentMatch);
    }
    private void butChatWithMatch_Click(object sender, EventArgs e)
    {
        Matching.allMadeMatches.Add(Matching.CurrentMatch.Username);
        this.Hide();
        Form chatPage = new chatPage();
        chatPage.ShowDialog();
        this.Close();
    }

    #region Page Buttons || These allow the user to switches to the other menus.
    private void matchHome_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form menuPage = new menuPage();
        menuPage.ShowDialog();
        this.Close();
    }
    public void matchChat_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form chatPage = new chatPage();
        chatPage.ShowDialog();
        this.Close();
    }
    private void matchHelp_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form helpPage = new helpPage();
        helpPage.ShowDialog();
        this.Close();
    }
    private void matchLogin_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form loginPage = new loginPage();
        loginPage.ShowDialog();
        this.Close();
    }
    #endregion
}