﻿namespace Dating_App;

internal partial class menuPage : Form
{
    public menuPage()
    {
        InitializeComponent(); ;
        DisplayProfile();
    }
    void DisplayProfile()
    {
        pUsername.Text = Database.CurrentUser.Username;
        pName.Text = Database.CurrentUser.FullName;
        pBirthdate.Text = Database.CurrentUser.DateOfBirth.ToString();
        pGender.Text = Database.CurrentUser.Gender;
        pEmail.Text = $"{Database.CurrentUser.Username}@gmail.com";
        Tools.displayImages(userDrawn, Database.CurrentUser.UserImages);
        Tools.displayProfilePictures(userPFP, Database.CurrentUser.UserImages);
        Tools.displayPFP(pictureBox1);
    }
    void drawingButtonsClick(object sender, EventArgs e)
    {
        PaintForm draw = new PaintForm($"{((PictureBox)sender).Tag}");
        draw.ShowDialog();
        Tools.displayImages(userDrawn, Database.CurrentUser.UserImages);
    }
    void displayImagesOnLoad(object sender, EventArgs e)
    {
        Tools.displayImages(userDrawn, Database.CurrentUser.UserImages);
    }

    #region Page Buttons || These allow the user to switch to the other menus.
    private void menuMatch_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form matchPage = new matchPage();
        matchPage.ShowDialog();
        this.Close();
    }
    private void menuChat_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form chatPage = new chatPage();
        chatPage.ShowDialog();
        this.Close();

    }

    private void menuHelp_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form helpPage = new helpPage();
        helpPage.ShowDialog();
        this.Close();
    }

    private void menuLogin_Click(object sender, EventArgs e)
    {
        this.Hide();
        Form loginPage = new loginPage();
        loginPage.ShowDialog();
        this.Close();
    }
    #endregion

    private void gender_Click(object sender, EventArgs e)
    {

    }
}