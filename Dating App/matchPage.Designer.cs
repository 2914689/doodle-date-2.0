﻿namespace Dating_App
{
    partial class matchPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.matchLogin = new System.Windows.Forms.Button();
            this.matchHelp = new System.Windows.Forms.Button();
            this.matchChat = new System.Windows.Forms.Button();
            this.matchMatch = new System.Windows.Forms.Button();
            this.matchHome = new System.Windows.Forms.Button();
            this.butMakeMatch = new System.Windows.Forms.Button();
            this.butChatWithMatch = new System.Windows.Forms.Button();
            this.pGender = new System.Windows.Forms.Label();
            this.pBirthdate = new System.Windows.Forms.Label();
            this.pName = new System.Windows.Forms.Label();
            this.userDrawn = new System.Windows.Forms.GroupBox();
            this.interest0 = new System.Windows.Forms.PictureBox();
            this.interest7 = new System.Windows.Forms.PictureBox();
            this.interest6 = new System.Windows.Forms.PictureBox();
            this.interest5 = new System.Windows.Forms.PictureBox();
            this.interest4 = new System.Windows.Forms.PictureBox();
            this.interest3 = new System.Windows.Forms.PictureBox();
            this.interest2 = new System.Windows.Forms.PictureBox();
            this.interest1 = new System.Windows.Forms.PictureBox();
            this.gender = new System.Windows.Forms.Label();
            this.Birthdate = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.profilePicture = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.userPFP = new System.Windows.Forms.GroupBox();
            this.userProfile = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.userDrawn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.interest0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.profilePicture)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.userPFP.SuspendLayout();
            this.userProfile.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // matchLogin
            // 
            this.matchLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.matchLogin.BackgroundImage = global::Dating_App.Properties.Resources.logouticon;
            this.matchLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.matchLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.matchLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.matchLogin.Location = new System.Drawing.Point(0, 560);
            this.matchLogin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.matchLogin.Name = "matchLogin";
            this.matchLogin.Size = new System.Drawing.Size(400, 140);
            this.matchLogin.TabIndex = 9;
            this.matchLogin.UseVisualStyleBackColor = false;
            this.matchLogin.Click += new System.EventHandler(this.matchLogin_Click);
            // 
            // matchHelp
            // 
            this.matchHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.matchHelp.BackgroundImage = global::Dating_App.Properties.Resources.helpicon;
            this.matchHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.matchHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.matchHelp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.matchHelp.Location = new System.Drawing.Point(0, 420);
            this.matchHelp.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.matchHelp.Name = "matchHelp";
            this.matchHelp.Size = new System.Drawing.Size(400, 140);
            this.matchHelp.TabIndex = 8;
            this.matchHelp.UseVisualStyleBackColor = false;
            this.matchHelp.Click += new System.EventHandler(this.matchHelp_Click);
            // 
            // matchChat
            // 
            this.matchChat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.matchChat.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.matchChat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.matchChat.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.matchChat.Image = global::Dating_App.Properties.Resources.chaticon;
            this.matchChat.Location = new System.Drawing.Point(0, 280);
            this.matchChat.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.matchChat.Name = "matchChat";
            this.matchChat.Size = new System.Drawing.Size(400, 140);
            this.matchChat.TabIndex = 7;
            this.matchChat.UseVisualStyleBackColor = false;
            this.matchChat.Click += new System.EventHandler(this.matchChat_Click);
            // 
            // matchMatch
            // 
            this.matchMatch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.matchMatch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.matchMatch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.matchMatch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.matchMatch.Image = global::Dating_App.Properties.Resources.matchicon;
            this.matchMatch.Location = new System.Drawing.Point(0, 140);
            this.matchMatch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.matchMatch.Name = "matchMatch";
            this.matchMatch.Size = new System.Drawing.Size(400, 140);
            this.matchMatch.TabIndex = 6;
            this.matchMatch.UseVisualStyleBackColor = false;
            // 
            // matchHome
            // 
            this.matchHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.matchHome.BackgroundImage = global::Dating_App.Properties.Resources.homeicon;
            this.matchHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.matchHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.matchHome.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.matchHome.Location = new System.Drawing.Point(0, 0);
            this.matchHome.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.matchHome.Name = "matchHome";
            this.matchHome.Size = new System.Drawing.Size(400, 140);
            this.matchHome.TabIndex = 5;
            this.matchHome.UseVisualStyleBackColor = false;
            this.matchHome.Click += new System.EventHandler(this.matchHome_Click);
            // 
            // butMakeMatch
            // 
            this.butMakeMatch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.butMakeMatch.FlatAppearance.BorderColor = System.Drawing.Color.DodgerBlue;
            this.butMakeMatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butMakeMatch.Location = new System.Drawing.Point(538, 814);
            this.butMakeMatch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.butMakeMatch.Name = "butMakeMatch";
            this.butMakeMatch.Size = new System.Drawing.Size(339, 100);
            this.butMakeMatch.TabIndex = 10;
            this.butMakeMatch.Text = "Show New Match";
            this.butMakeMatch.UseVisualStyleBackColor = false;
            this.butMakeMatch.Click += new System.EventHandler(this.makeMatch_Click);
            // 
            // butChatWithMatch
            // 
            this.butChatWithMatch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.butChatWithMatch.FlatAppearance.BorderColor = System.Drawing.Color.DodgerBlue;
            this.butChatWithMatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butChatWithMatch.Location = new System.Drawing.Point(1418, 814);
            this.butChatWithMatch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.butChatWithMatch.Name = "butChatWithMatch";
            this.butChatWithMatch.Size = new System.Drawing.Size(339, 100);
            this.butChatWithMatch.TabIndex = 11;
            this.butChatWithMatch.Text = "Chat with match(prop)";
            this.butChatWithMatch.UseVisualStyleBackColor = false;
            this.butChatWithMatch.Click += new System.EventHandler(this.butChatWithMatch_Click);
            // 
            // pGender
            // 
            this.pGender.AutoSize = true;
            this.pGender.BackColor = System.Drawing.Color.Transparent;
            this.pGender.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pGender.Location = new System.Drawing.Point(147, 119);
            this.pGender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pGender.Name = "pGender";
            this.pGender.Size = new System.Drawing.Size(68, 21);
            this.pGender.TabIndex = 24;
            this.pGender.Text = "Gender:";
            // 
            // pBirthdate
            // 
            this.pBirthdate.AutoSize = true;
            this.pBirthdate.BackColor = System.Drawing.Color.Transparent;
            this.pBirthdate.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pBirthdate.Location = new System.Drawing.Point(147, 82);
            this.pBirthdate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pBirthdate.Name = "pBirthdate";
            this.pBirthdate.Size = new System.Drawing.Size(82, 21);
            this.pBirthdate.TabIndex = 23;
            this.pBirthdate.Text = "Birthdate:";
            // 
            // pName
            // 
            this.pName.AutoSize = true;
            this.pName.BackColor = System.Drawing.Color.Transparent;
            this.pName.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pName.Location = new System.Drawing.Point(147, 46);
            this.pName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pName.Name = "pName";
            this.pName.Size = new System.Drawing.Size(60, 21);
            this.pName.TabIndex = 21;
            this.pName.Text = "Name: ";
            // 
            // userDrawn
            // 
            this.userDrawn.BackColor = System.Drawing.Color.AliceBlue;
            this.userDrawn.Controls.Add(this.interest0);
            this.userDrawn.Controls.Add(this.interest7);
            this.userDrawn.Controls.Add(this.interest6);
            this.userDrawn.Controls.Add(this.interest5);
            this.userDrawn.Controls.Add(this.interest4);
            this.userDrawn.Controls.Add(this.interest3);
            this.userDrawn.Controls.Add(this.interest2);
            this.userDrawn.Controls.Add(this.interest1);
            this.userDrawn.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.userDrawn.Location = new System.Drawing.Point(443, 98);
            this.userDrawn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.userDrawn.Name = "userDrawn";
            this.userDrawn.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.userDrawn.Size = new System.Drawing.Size(660, 475);
            this.userDrawn.TabIndex = 19;
            this.userDrawn.TabStop = false;
            this.userDrawn.Text = "Their Gallery";
            // 
            // interest0
            // 
            this.interest0.BackColor = System.Drawing.Color.White;
            this.interest0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.interest0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest0.Location = new System.Drawing.Point(19, 45);
            this.interest0.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest0.Name = "interest0";
            this.interest0.Size = new System.Drawing.Size(151, 196);
            this.interest0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest0.TabIndex = 18;
            this.interest0.TabStop = false;
            this.interest0.Tag = "0";
            // 
            // interest7
            // 
            this.interest7.BackColor = System.Drawing.Color.White;
            this.interest7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.interest7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest7.Location = new System.Drawing.Point(491, 250);
            this.interest7.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest7.Name = "interest7";
            this.interest7.Size = new System.Drawing.Size(151, 196);
            this.interest7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest7.TabIndex = 17;
            this.interest7.TabStop = false;
            this.interest7.Tag = "7";
            // 
            // interest6
            // 
            this.interest6.BackColor = System.Drawing.Color.White;
            this.interest6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.interest6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest6.Location = new System.Drawing.Point(334, 250);
            this.interest6.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest6.Name = "interest6";
            this.interest6.Size = new System.Drawing.Size(151, 196);
            this.interest6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest6.TabIndex = 16;
            this.interest6.TabStop = false;
            this.interest6.Tag = "6";
            // 
            // interest5
            // 
            this.interest5.BackColor = System.Drawing.Color.White;
            this.interest5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.interest5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest5.Location = new System.Drawing.Point(176, 250);
            this.interest5.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest5.Name = "interest5";
            this.interest5.Size = new System.Drawing.Size(151, 196);
            this.interest5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest5.TabIndex = 15;
            this.interest5.TabStop = false;
            this.interest5.Tag = "5";
            // 
            // interest4
            // 
            this.interest4.BackColor = System.Drawing.Color.White;
            this.interest4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.interest4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest4.Location = new System.Drawing.Point(19, 250);
            this.interest4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest4.Name = "interest4";
            this.interest4.Size = new System.Drawing.Size(151, 196);
            this.interest4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest4.TabIndex = 14;
            this.interest4.TabStop = false;
            this.interest4.Tag = "4";
            // 
            // interest3
            // 
            this.interest3.BackColor = System.Drawing.Color.White;
            this.interest3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.interest3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest3.Location = new System.Drawing.Point(491, 45);
            this.interest3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest3.Name = "interest3";
            this.interest3.Size = new System.Drawing.Size(151, 196);
            this.interest3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest3.TabIndex = 13;
            this.interest3.TabStop = false;
            this.interest3.Tag = "3";
            // 
            // interest2
            // 
            this.interest2.BackColor = System.Drawing.Color.White;
            this.interest2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.interest2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest2.Location = new System.Drawing.Point(334, 45);
            this.interest2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest2.Name = "interest2";
            this.interest2.Size = new System.Drawing.Size(151, 196);
            this.interest2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest2.TabIndex = 12;
            this.interest2.TabStop = false;
            this.interest2.Tag = "2";
            // 
            // interest1
            // 
            this.interest1.BackColor = System.Drawing.Color.White;
            this.interest1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.interest1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.interest1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.interest1.Location = new System.Drawing.Point(176, 45);
            this.interest1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.interest1.Name = "interest1";
            this.interest1.Size = new System.Drawing.Size(151, 196);
            this.interest1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.interest1.TabIndex = 11;
            this.interest1.TabStop = false;
            this.interest1.Tag = "1";
            // 
            // gender
            // 
            this.gender.AutoSize = true;
            this.gender.BackColor = System.Drawing.Color.Transparent;
            this.gender.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.gender.Location = new System.Drawing.Point(61, 119);
            this.gender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.gender.Name = "gender";
            this.gender.Size = new System.Drawing.Size(68, 21);
            this.gender.TabIndex = 18;
            this.gender.Text = "Gender:";
            // 
            // Birthdate
            // 
            this.Birthdate.AutoSize = true;
            this.Birthdate.BackColor = System.Drawing.Color.Transparent;
            this.Birthdate.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Birthdate.Location = new System.Drawing.Point(59, 82);
            this.Birthdate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Birthdate.Name = "Birthdate";
            this.Birthdate.Size = new System.Drawing.Size(82, 21);
            this.Birthdate.TabIndex = 17;
            this.Birthdate.Text = "Birthdate:";
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.BackColor = System.Drawing.Color.Transparent;
            this.name.Font = new System.Drawing.Font("Candara", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.name.Location = new System.Drawing.Point(59, 46);
            this.name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(60, 21);
            this.name.TabIndex = 16;
            this.name.Text = "Name: ";
            // 
            // profilePicture
            // 
            this.profilePicture.BackColor = System.Drawing.Color.White;
            this.profilePicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.profilePicture.Location = new System.Drawing.Point(59, 22);
            this.profilePicture.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.profilePicture.Name = "profilePicture";
            this.profilePicture.Size = new System.Drawing.Size(140, 171);
            this.profilePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.profilePicture.TabIndex = 15;
            this.profilePicture.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.groupBox1.Controls.Add(this.userPFP);
            this.groupBox1.Controls.Add(this.userProfile);
            this.groupBox1.Controls.Add(this.userDrawn);
            this.groupBox1.Location = new System.Drawing.Point(538, 130);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(1219, 669);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            // 
            // userPFP
            // 
            this.userPFP.BackColor = System.Drawing.Color.AliceBlue;
            this.userPFP.Controls.Add(this.profilePicture);
            this.userPFP.Location = new System.Drawing.Point(109, 98);
            this.userPFP.Name = "userPFP";
            this.userPFP.Size = new System.Drawing.Size(279, 226);
            this.userPFP.TabIndex = 26;
            this.userPFP.TabStop = false;
            // 
            // userProfile
            // 
            this.userProfile.BackColor = System.Drawing.Color.AliceBlue;
            this.userProfile.Controls.Add(this.name);
            this.userProfile.Controls.Add(this.pGender);
            this.userProfile.Controls.Add(this.Birthdate);
            this.userProfile.Controls.Add(this.pBirthdate);
            this.userProfile.Controls.Add(this.gender);
            this.userProfile.Controls.Add(this.pName);
            this.userProfile.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.userProfile.Location = new System.Drawing.Point(109, 330);
            this.userProfile.Name = "userProfile";
            this.userProfile.Size = new System.Drawing.Size(279, 243);
            this.userProfile.TabIndex = 25;
            this.userProfile.TabStop = false;
            this.userProfile.Text = "Your Match";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.matchHome);
            this.panel1.Controls.Add(this.matchMatch);
            this.panel1.Controls.Add(this.matchChat);
            this.panel1.Controls.Add(this.matchLogin);
            this.panel1.Controls.Add(this.matchHelp);
            this.panel1.Location = new System.Drawing.Point(0, 340);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 740);
            this.panel1.TabIndex = 26;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(217)))), ((int)(((byte)(243)))));
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 340);
            this.panel2.TabIndex = 27;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(100, 90);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Dating_App.Properties.Resources.DoodleDateLogo1;
            this.pictureBox2.Location = new System.Drawing.Point(0, -17);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(400, 156);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // matchPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.butChatWithMatch);
            this.Controls.Add(this.butMakeMatch);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "matchPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "matchPage";
            this.Load += new System.EventHandler(this.matchPage_Load);
            this.userDrawn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.interest0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.interest1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.profilePicture)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.userPFP.ResumeLayout(false);
            this.userProfile.ResumeLayout(false);
            this.userProfile.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button matchLogin;
        private System.Windows.Forms.Button matchHelp;
        private System.Windows.Forms.Button matchChat;
        private System.Windows.Forms.Button matchMatch;
        private System.Windows.Forms.Button matchHome;
        private System.Windows.Forms.Button butMakeMatch;
        private System.Windows.Forms.Button butChatWithMatch;
        private System.Windows.Forms.Label pGender;
        private System.Windows.Forms.Label pBirthdate;
        private System.Windows.Forms.Label pName;
        private System.Windows.Forms.GroupBox userDrawn;
        private System.Windows.Forms.PictureBox interest0;
        private System.Windows.Forms.Label gender;
        private System.Windows.Forms.Label Birthdate;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.PictureBox profilePicture;
        private System.Windows.Forms.PictureBox interest7;
        private System.Windows.Forms.PictureBox interest6;
        private System.Windows.Forms.PictureBox interest5;
        private System.Windows.Forms.PictureBox interest4;
        private System.Windows.Forms.PictureBox interest3;
        private System.Windows.Forms.PictureBox interest2;
        private System.Windows.Forms.PictureBox interest1;
        private System.Windows.Forms.GroupBox groupBox1;
        private Panel panel1;
        private Panel panel2;
        private GroupBox userProfile;
        private PictureBox pictureBox1;
        private GroupBox userPFP;
        private PictureBox pictureBox2;
    }
}