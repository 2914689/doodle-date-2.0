<p align="center"><img src="https://cdn.discordapp.com/attachments/762288578382135326/1068485787668205568/appicon.png" alt="Logo" width="200"></p>
<h1 align="center">Doodle Date  <br>

Welcome to Doodle Date, a dating application in which drawings are the central concept. DoodleDate is a blind dating application 
in which users are not able to use any kind of photographs to customize their profile. Instead, users are encouraged to let their creativity flow and make drawings.

These can be of themselves, what they’re passionate about, or what interests them and will be visible on the user's profile for others to see and serve as a conversation starter with any potential match. The matching algorithm we built does use regular data such as age preference and sexuality, but besides filling them in during registration this data is never visible to any of the users.

## About Us & The Project
We are a group of 5 undergraduate IT students at Utrecht University and this is our first group project. We wanted to make a dating app that didn't follow the hard pattern that has been set by similar applications over the last decade, as we feel that those platforms are too "looks" based and don't focus on personality all, from there the idea to make a dating app centered around user-made drawings. We praise our originality but as first year students with yet limited programming knowledge (mostly C#) we definitely faced some difficulties.

## Running The Program
You can run Doodle Date by downloading the DoodleDate.zip file in the main directory, extracting it anywhere and running the executable, it should not have any other dependencies.

## Known Issues
* When changing or adding a drawing it doesn't always save correctly.
* The chatfeature doesn't work the way we intended it too and requires some manual setup for actual chatting.
* The profilepicture is displayed twice
* Removing a certain interest while registering blocks it from being selected again 

## The Chat Feature
As stated in [Known Issues](#known-issues) getting the chat to work requires some manual setup. It should be reiterated that the chat feature works purely on LAN connections due to our limited knowledge of network code. Activating the chat requires manually setting the IP and Port of the person you are trying to communicate with. See the following video:

<p align="center"><img src=https://cdn.discordapp.com/attachments/762288578382135326/1068490791502217266/ChatFunction.mp4></p>

<ins>Notes<ins>
- The Local IP is always correct and should never be changed. The <ins>Remote IP is the other user's Local IP.<ins> Ask your friend what their Local IP is.
- ^ If you're wondering why I don't do this in the video that's because there's 2 instance being run on the same device, meaning the Local and Remote IP are the same.
- The ports are mutually exclusive, you cannot bot share the same Local or Remote Port, in stead you must invert them. <br /> Example: User 1 has Local Port = 80 & Remote Port = 81 --> User 2 should have Local Port = 81 & Remote Port = 80.

## Disclaimer
This is an Undergraduate project, assume that the latest version is the final version as we probably won't maintain or support the program after the course ends, that said, if you have any other questions feel free to contact Beau about it through the e-mail on his Git Profile.

## Contacts
* [GitLab](https://git.science.uu.nl/2914689/doodle-date-2.0)
* [Beau's Git Profile](https://git.science.uu.nl/2914689)

## License
- [![License](https://img.shields.io/badge/License-MIT-red.svg?style=flat-square)](http://opensource.org/licenses/MIT)

## Credits
* Utrecht University for introducing and giving us and our fellow students access to all the tools we need.
